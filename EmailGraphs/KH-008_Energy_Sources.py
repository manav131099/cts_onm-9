import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import pyodbc
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatch
import matplotlib.backends.backend_pdf


path='/home/admin/Dropbox/Gen 1 Data/[KH-008X]/'
meters=["Pond", "R06", "R11", "R02", "R03"]
final=pd.DataFrame(columns=['Date','Power','Type'])
path_write='/home/admin/Graphs/'

date=coddate=sys.argv[1]
next_date=datetime.datetime.strptime(date, "%Y-%m-%d")+datetime.timedelta(days=1) #Addding one for break point.
df_final=pd.DataFrame(columns=['Date','Energy','Type'])
for k in sorted(os.listdir(path)):
    for i in sorted(os.listdir(path+k)):
            if('WHR' not in os.listdir(path+k+'/'+i)):
                continue
            else:
                for j in sorted(os.listdir(path+k+'/'+i)):
                    for n in sorted(os.listdir(path+k+'/'+i+'/'+j)):
                        if(n.split(' ')[1]==str(next_date.date())+'.txt'):
                            print('break')
                            print(n)
                            break
                        df_new=pd.DataFrame(columns=['Date','Energy','Type'])
                        df=pd.read_csv(path+k+'/'+i+'/'+j+'/'+n,sep='\t')
                        if(j=='WHR'):
                            df_new['Energy']=[(df['customId_Active_Energy_Received_.Out_of_Load...Wh.'].dropna().iloc[len(df['customId_Active_Energy_Received_.Out_of_Load...Wh.'].dropna())-1]-df['customId_Active_Energy_Received_.Out_of_Load...Wh.'].dropna().iloc[0])/1000000]
                            df_new['Date']=[df['Local.Time.Stamp'].iloc[0][0:10]]
                            df_new['Type']=["Heat"]
                        elif(j=='Load'):
                            df_new['Energy']=[df['Active.Power..W.'].sum()/60000000]
                            df_new['Date']=[df['Local.Time.Stamp'].iloc[0][0:10]]
                            df_new['Type']=['Load']
                        else:
                            df_new['Energy']=[df['Active.Power..W.'].sum()/60000000]
                            df_new['Date']=[df['Local.Time.Stamp'].iloc[0][0:10]]
                            df_new['Type']=['Solar']
                        df_final=df_final.append(df_new,sort=False)



plt.rcParams['axes.facecolor'] = 'gainsboro'


df=df_final
df.loc[df['Type']=='Heat','Energy']=df.loc[df['Type']=='Heat','Energy']

pivot_df = df.pivot_table(index='Date', columns='Type', values='Energy',aggfunc='sum')
pivot_df=pivot_df.fillna(0)
pivot_df.loc[pivot_df['Solar']<.1,:]=0

pivot_df['Total']=pivot_df['Solar']+pivot_df['Heat']+pivot_df['Load']
pivot_df['WHR&Load']=(pivot_df['Heat']/pivot_df['Load'])*100
pivot_df['Solar&Load']=(pivot_df['Solar']/pivot_df['Load'])*100
pivot_df['Load_Contribution']=(pivot_df['Load']/pivot_df['Total'])*100
pivot_df['Heat_Contribution']=(pivot_df['Heat']/pivot_df['Total'])*100
pivot_df['Solar_Contribution']=(pivot_df['Solar']/pivot_df['Total'])*100

pivot_df.index=pd.to_datetime(pivot_df.index)

pivot_df =pivot_df.resample("1d").mean()
pivot_df.to_csv(path_write+"Graph_Extract/KH-008/[KH-008] Graph "+str(df_final['Date'].values.tolist()[len(df_final)-1])+" - CMIC Electricity Sources.txt",sep='\t',index=True)
lifetime_points=len(pivot_df.index.drop_duplicates())
lifetime_avg_solar=pivot_df['Solar_Contribution'].mean()
lifetime_avg_heat=pivot_df['Heat_Contribution'].mean()
lifetime_avg_loads=pivot_df['Total'].mean()
full_pivot_df=pivot_df.resample("1d").mean().tail(60)
month_pivot_df=pivot_df.groupby(pd.Grouper(freq='M'))
colors = ["purple", "orange", "yellow"]
colors2 = ["orange", "yellow"]


def plot(pivot_df,Type):
    totals=pivot_df['Total'].values.round().astype(int).tolist()
    avg_heat=pivot_df['Heat_Contribution'].mean()
    avg_solar=pivot_df['Solar_Contribution'].mean()
    avg_loads=pivot_df['Total'].mean()
    if(Type=='monthly'):
        pivot_df.index=pivot_df.index.strftime("%Y-%m-%d")
    else:
        pivot_df.index=pivot_df.index.strftime("%Y-%m-%d")
    fig, ax = plt.subplots()
    lns1=pivot_df.loc[:,['Load','Heat', 'Solar']].plot.bar(stacked=True, color=colors,ax=ax,edgecolor = "none")
    plt.axhline(y=100, color='black', linestyle='dashed')
    plt.axhline(y=200, color='black', linestyle='dashed')
    plt.axhline(y=300, color='black', linestyle='dashed')
    plt.axhline(y=400, color='black', linestyle='dashed')
    plt.axhline(y=500, color='black', linestyle='dashed')
    plt.axhline(y=600, color='black', linestyle='dashed')
    plt.axhline(y=700, color='black', linestyle='dashed')
    xlims = ax.get_xlim()
    ax2 = ax.twinx()
    lns2=pivot_df.loc[:,['Heat_Contribution', 'Solar_Contribution']].plot(ax=ax2, color=colors2,marker='o',figsize=(55,25),ms=10,linewidth=2)
    ax.set_ylim(0,750)
    ax2.set_ylim(0,100)
    ax2.set_xlim(xlims)
    ttl_main=fig.suptitle('[KH-008] Factory Loads vs. Sources of Electricity Production on Site',fontsize=40)

    print(pivot_df.head(1))
    print(pivot_df.tail(1))
    ttl = ax.set_title('From '+ str(pivot_df.index.values[0]) +' to '+ str(pivot_df.index.values[len(pivot_df.index.values)-1]), fontdict={'fontsize': 35, 'fontweight': 'medium'})
    ttl.set_position([.5, 1.02])
    # setting label sizes after creation
    ax.yaxis.label.set_size(20)
    ax2.yaxis.label.set_size(20)
    ax.grid(False)
    ax2.grid(False)
    L=ax.legend(loc='upper right',prop={'size': 20})
    ax2.get_legend().remove()
    ax2.yaxis.set_label_coords(1.02,.5)
    L.get_texts()[0].set_text('Mains Intake')
    L.get_texts()[1].set_text('Waste Heat Recovery')
    L.get_texts()[2].set_text('Solar PV')
    ax.tick_params(axis='both', which='major', labelsize=20.5)
    ax2.tick_params(axis='both', which='major', labelsize=20.5)
    if(Type=='monthly'):
        for i, label in enumerate(totals):
            if(label==0 or label<0):
                ax.annotate('DATA NOT AVAILABLE', (i-.12, 118), rotation=90,size=22,color='red')
            else:
                ax.annotate(str(label), (i-.12, (label)+14), rotation=90,size=22)
        plt.text(0.6, 0.388,'Average WHR Contribution [%]: '+str(round(avg_heat,1)), fontsize=18, transform=plt.gcf().transFigure,weight='bold',color='orange', bbox=dict(facecolor='black', alpha=0.8,pad=10.0))
        plt.text(.443, 0.188,'Average PV Contribution [%]: '+str(round(avg_solar,1)), fontsize=18, transform=plt.gcf().transFigure,weight='bold',color='yellow', bbox=dict(facecolor='black', alpha=0.8,pad=10.0))
        pivot_df= pivot_df[pivot_df['Total'] != 0]
        ax.annotate('No. of Points: '+ str(len(pivot_df['Total'].dropna()))+' days', (.01, .98),xytext=(4, -4),xycoords='axes fraction',textcoords='offset points',fontweight='bold',color='white',backgroundcolor='k',ha='left', va='top',size=20)
        ax.annotate('Average Daily Loads [MWh]: '+str(round(avg_loads,1)), (.1, .98),xytext=(4, -4),xycoords='axes fraction',textcoords='offset points',fontweight='bold',color='white',backgroundcolor='k',ha='left', va='top',size=20)
    else:
        for i, label in enumerate(totals):
            if(label==0 or label<0):
                ax.annotate('DATA NOT AVAILABLE', (i-.20, 98), rotation=90,size=18,color='red')
            else:
                ax.annotate(str(label), (i-.20, (label)+10), rotation=90,size=18)
        plt.text(0.6, 0.388,'Lifetime Average WHR Contribution [%]: '+str(round(lifetime_avg_heat,1)), fontsize=18, transform=plt.gcf().transFigure,weight='bold',color='orange', bbox=dict(facecolor='black', alpha=0.8,pad=10.0))
        plt.text(.443, 0.188,'Lifetime Average PV Contribution [%]: '+str(round(lifetime_avg_solar,1)), fontsize=18, transform=plt.gcf().transFigure,weight='bold',color='yellow', bbox=dict(facecolor='black', alpha=0.8,pad=10.0))
        pivot_df= pivot_df[pivot_df['Total'] != 0]
        ax.annotate('Lifetime No. of Points: '+ str(lifetime_points)+' days', (.01, .98),xytext=(4, -4),xycoords='axes fraction',textcoords='offset points',fontweight='bold',color='white',backgroundcolor='k',ha='left', va='top',size=20)
        ax.annotate('Lifetime Average Daily Loads [MWh]: '+str(round(lifetime_avg_loads,1)), (.15, .98),xytext=(4, -4),xycoords='axes fraction',textcoords='offset points',fontweight='bold',color='white',backgroundcolor='k',ha='left', va='top',size=20)
    ax.set_ylabel('Energy [MWh]', fontsize=25)
    ax2.set_ylabel('Contribution to Factory Loads [%]', fontsize=25,rotation=270)
    
    return fig

pdf = matplotlib.backends.backend_pdf.PdfPages(path_write+"Graph_Output/KH-008/[KH-008] Graph Monthly - CMIC Electricity Sources.pdf")
for i in month_pivot_df:
    pivot_df=i[1]
    fig=plot(pivot_df,"monthly")
    pdf.savefig(fig, bbox_inches='tight')
pdf.close()

fig=plot(full_pivot_df,"not_monthly")
fig.savefig(path_write+"Graph_Output/KH-008/[KH-008] Graph "+str(full_pivot_df.index.values.tolist()[len(full_pivot_df)-1])+" - CMIC Electricity Sources.pdf", bbox_inches='tight')

""" for index,i in enumerate(month_pivot_df):
    pivot_df=i[1]
    fig=plot(pivot_df)
    fig.savefig("KH-008_Sources"+str(index)+".pdf", bbox_inches='tight') """


