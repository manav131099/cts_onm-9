import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re
import time
import shutil
import pytz
import sys
import pyodbc
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatch
from matplotlib.dates import DateFormatter
import matplotlib.dates as mdates
from datetime import date
from datetime import timedelta

date_70 = list()
ga_70 = list()

d0 = date(2020, 1, 1)
d1 = date.today()
yesterday = d1 - timedelta(days = 1)
d1 = yesterday
d1 = sys.argv[1]
date_list = pd.date_range(start=d0,end=d1).tolist()
for i in range(0,len(date_list)):
    date_list[i] = str(date_list[i])[0:10]


da_fail_safe_dates = list()
irr_data_fail_safe = [0]*288
for i in range(72,219):
    irr_data_fail_safe[i] = 30


start_date = '2020-01-01'
last_date = str(d1)[0:10]
path = "/home/admin/Dropbox/Gen 1 Data/[IN-081L]"
years = os.listdir(path)

GA_list = [-1]*len(date_list)
DA_MFM_List = [-1]*len(date_list)
DA_WMS_List = [-1]*len(date_list)

for year in years:
    if(int(year) <2020):
        continue
    year_mons = os.listdir(path+'/'+year)

    for year_mon in year_mons:
        devices = os.listdir(path+'/'+year+'/'+year_mon)
        for device in devices:
            if(device == 'MFM_1_PV MFM'):
                device_path = path+'/'+year+'/'+year_mon+'/'+device
                files = os.listdir(device_path)
                for file in files:
                    date_year = int(file[15:19])
                    date_month = int(file[20:22])
                    date_day = int(file[23:25])

                    end_date = date(date_year,date_month,date_day)
                    date_idx = (end_date-d0).days

                    if(date_year == 2019):
                        continue
                    if(file[15:25] == str(date.today())):
                        continue

                    path_2g = "/home/admin/Dropbox/Gen 1 Data/[IN-081L]/"+year+'/'+year_mon+'/'+device+'/'+file
                    if(os.path.exists(path_2g) == False):
                        continue
                        # da_file = pd.read_csv(path_2g,delimiter='\t')
                        # count_da_check = da_file['Hz_avg'].isna().sum()
                        # da = (288-count_da_check)*100/288
                        # DA_MFM_List[date_idx] = da
                        #
                        # if(file[15:25] == '2020-01-04'):
                        #     print(da)
                        #
                        # if(float(da)<90):
                        #     print(file[15:25])
                        #     continue

                    irridation_fail_safe = False
                    path_2g = "/home/admin/Dropbox/Gen 1 Data/[IN-081L]/"+year+'/'+year_mon+'/'+'WMS_1_IMT/'+'[IN-081L]-WMS1-'+file[15:25]+'.txt'
                    if(os.path.exists(path_2g) == True):
                        da_file = pd.read_csv(path_2g,delimiter='\t')
                        count_da_check = da_file['POAI_avg'].isna().sum()
                        da = (288-count_da_check)*100/288

                        if(file[15:25] == '2020-01-04'):
                            print(da)

                        DA_WMS_List[date_idx] = da
                        # da = da_file['DA'][0]
                        if(float(da)<50):
                            count_irr = 145
                            irridation_fail_safe = True
                            da_fail_safe_dates.append(file[15:25])
                    irr_list = list()
                    freq_list = list()
                    file_path = path+'/'+year+'/'+year_mon+'/'+device+'/'+file
                    if(os.path.exists(file_path) == False):
                        continue
                    dataread_MFM = pd.read_csv(file_path,delimiter='\t')
                    df_MFM = pd.DataFrame()
                    df_MFM['timestamps'] = dataread_MFM['ts']
                    df_MFM['freq_data'] = dataread_MFM['Hz_avg']


                    wms_path = path+'/'+year+'/'+year_mon+'/'+'WMS_1_IMT'+'/'+'[IN-081L]-WMS1-'+file[15:25]+'.txt'
                    if(os.path.exists(wms_path) == False):
                        continue
                    dataread_WMS = pd.read_csv(wms_path, delimiter='\t')
                    df_WMS = pd.DataFrame()
                    df_WMS['timestamps'] = dataread_WMS['ts']
                    df_WMS['irr_data'] = dataread_WMS['POAI_avg']
                    if(irridation_fail_safe == True):
                        df_WMS['irr_data'] = irr_data_fail_safe

                    df_t1 = df_WMS[df_WMS['irr_data']>=20]
                    df_WMS = df_WMS.merge(df_MFM, on='timestamps')
                    #73 to 219 as 30
                    df_t2 = df_WMS[((df_WMS['irr_data']>=20) & (df_WMS['freq_data']>=40))]

                    count_irr = len(df_t1)
                    count_freq = len(df_t2)

                    if(count_irr == 0):
                        ga=0
                        print(count_irr)
                    else:
                        ga = round((count_freq*100)/count_irr,2)
                        if(count_freq == 0):
                            ga = -1

                    if(ga<70):
                        date_70.append(file[15:25])
                        ga_70.append(ga)

                    end_date = date(date_year,date_month,date_day)
                    date_idx = (end_date-d0).days
                    GA_list[date_idx] = ga



print(len(GA_list))
print(len(date_list))

for i in range(0,len(date_list)-1):
    for j in range(0,len(date_list)-1):
        if(date_list[j]>date_list[j+1]):
            date = date_list[j]
            date_list[j] = date_list[j+1]
            date_list[j+1] = date

            x = GA_list[j]
            GA_list[j] = GA_list[j+1]
            GA_list[j+1] = x

ga_avg = 0

count_pos = len(date_list)

for i in range(0,len(date_list)):
    if(GA_list[i] == -1):
        count_pos = count_pos - 1
    else:
        ga_avg = ga_avg+GA_list[i]

ga_avg = ga_avg/count_pos

dates_datetime = [datetime.datetime.strptime(d,"%Y-%m-%d").date() for d in date_list]

df_mfm = pd.DataFrame()
df_mfm.insert(0,'Date',dates_datetime,True)
df_mfm.insert(1,'GA',GA_list,True)
df_mfm.insert(2,'DA_MFM',DA_MFM_List,True)
df_mfm.insert(3,'DA_WMS',DA_WMS_List,True)

df_mfm = df_mfm[((df_mfm['GA']>0))]
print(df_mfm.head(50))
date_list = df_mfm['Date']

ga_avg = df_mfm['GA'].sum()
ga_avg = ga_avg/len(df_mfm['GA'])

count_pos = len(df_mfm['GA'])
# for i in range(0,len(df_mfm['GA'])-1):
#     if(df_mfm['GA'][i]<50):
#         print(df_mfm['Date'][i])
#         print(df_mfm['GA'][i])
#         print('\n')

dates_datetime = [datetime.datetime.strptime(str(d),"%Y-%m-%d").date() for d in date_list]

dates_graph = list(df_mfm['Date'])
last_date = dates_graph[len(dates_graph)-1]
GA_Graph = list(df_mfm['GA'])

GA_Marking = GA_Graph
dates = dates_graph
font = {'size' : 30}

data_extract_path = '/home/admin/Graphs/Graph_Extract/IN-081/[IN-081] Graph '+str(last_date)+' - Grid Availability.txt'
df_mfm.to_csv(data_extract_path, sep = '\t', index=False, header=True)

path_final = '/home/admin/Graphs/Graph_Output/IN-081/[IN-081] Graph '+str(last_date)+' - Grid Availability.pdf'

plt.rcParams.update({'font.size': 30})
plt.rcParams['axes.facecolor'] = 'gainsboro'

fig = plt.figure(num=None, figsize=(50, 30))
ax = fig.add_subplot(111)


plt.plot(dates_graph,GA_Graph,marker='o',color='red',linewidth=5, label='MFM')

plt.xticks(dates_graph)
plt.ylim([0,105])

date_form = DateFormatter("%b-%y")
ax.xaxis.set_major_formatter(date_form)
ax.xaxis.set_major_locator(mdates.MonthLocator(interval=3))
ax.set(xlim=['2020-01-01',str(dates_datetime[len(dates_datetime)-1])])

ttl = ax.set_title(str(dates_graph[0])+' to '+str(last_date), fontdict={'fontsize': 55, 'fontweight': 'medium'})
ttl.set_position([.485, 1.02])
ttl_main=fig.suptitle('[IN-081L] Grid Availability',fontsize=70)

plt.legend()
legend = plt.legend(loc='best', ncol=1, prop={'size':50})

lifetime_ga_text = 'Lifetime GA: '+str(round(ga_avg,2))
no_of_points_txt = 'No. of Points: '+str(count_pos)+' days'
plt.text(0.15, 0.2,no_of_points_txt, fontsize=50, transform=plt.gcf().transFigure,weight='bold',color='white',bbox=dict(facecolor='black', alpha=0.8,pad=10.0))
plt.text(0.15, 0.25,lifetime_ga_text, fontsize=50, transform=plt.gcf().transFigure,weight='bold',color='white',bbox=dict(facecolor='black', alpha=0.8,pad=10.0))


for index,i in enumerate(GA_Marking):
    if(i<70):
        ln1=plt.scatter(dates[index], i, marker='D',color='green',s=300)


for i, label in enumerate(dates):
    if(GA_Marking[i]<70):
        ax.annotate(label, (label, GA_Marking[i]+.25),size=25,color='green')

ax.set_ylabel('Grid Availability [%]', fontsize=60)
ax.set_xlabel('Date', fontsize=60)
fig.savefig(path_final, bbox_inches='tight')
