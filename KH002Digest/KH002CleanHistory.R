rm(list = ls())
errHandle = file('/home/admin/Logs/LogsKH002History.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

require('mailR')
source('/home/admin/CODE/KH002Digest/FTPKH002Dump.R')
source('/home/admin/CODE/Misc/memManage.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
FIREERRATA = c(1,1)
LTCUTOFF = 0.001
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

timetomins = function(x)
{
  hrs = unlist(strsplit(x,":"))
  mins = as.numeric(hrs[2])
  hrs = as.numeric(hrs[1]) * 60
  return((((hrs + mins)/5)+1))
}

checkErrata = function(row,ts,no)
{
	{
  if(no == 1)
	  no2 = 'Solar'
	else if(no == 2)
	  no2 = 'Loads'
	}
  if(ts < 108 || ts > 204 || no == 2)
	{return()}
	if(FIREERRATA[no] == 0)
	{
	  print(paste('Errata mail already sent for meter',no,'so no more mails'))
		return()
	}
	if((!(is.finite(as.numeric(row[2])))) || (as.numeric(row[2]) < LTCUTOFF))
	{
		FIREERRATA[no] <<- 0
		subj = paste('KH-002X',no2,'down')
    body = 'Last recorded timestamp and reading\n'
		body = paste(body,'Timestamp:',as.character(row[1]))
		body = paste(body,'Real Power Tot kW reading:',as.character(row[2]))
		send.mail(from = 'operations@cleantechsolar.com',
							to = getRecipients("KH-002X","a"),
							subject = subj,
							body = body,
							smtp = list(host.name = "smtp.office365.com", port = 587, user.name = 'shravan.karthik@cleantechsolar.com',passwd = 'CTS&*(789', tls = TRUE),
							authenticate = TRUE,
							send = TRUE,
							debug = F)
		print(paste('Errata mail sent meter ',no))
		msgbody = paste(subj,"\n Timestamp:",as.character(row[1]),
		                "\n Real Pow Tot kW reading:",as.character(row[2]))
		command = paste('python /home/admin/CODE/Send_mail/twilio_alert.py',' "',msgbody,'"',' "KH-002X"',sep = "")
		system(command)
		print('Twilio message fired')
		recordTimeMaster("KH-002X","TwilioAlert",as.character(row[1]))
	}
}

stitchFile = function(path,days,pathwrite,erratacheck)
{
x = 1
t = try(read.csv(paste(path,days[x],sep="/"),header = T, skip=6,stringsAsFactors=F),silent = T)
  if(length(t)==1)
        {
                print(paste("Skipping",days[x],"Due to err in file"))
                return()
        }
  dataread = t
        if(nrow(dataread) < 1)
                return()
  dataread = dataread[,-c(1,2)]
  currcolnames = colnames(dataread)
  idxvals = match(currcolnames,colnames)
  temp = unlist(strsplit(days[x]," "))
        temp = unlist(strsplit(temp[2],"_"))
  temp = as.numeric(temp[1])
  for(y in 1 : nrow(dataread))
  {
    idxts = timetomins(substr(as.character(dataread[y,1]),12,16))
    if(!is.finite(idxts))
    {
      print(paste('Skipping row',y,'as incorrect timestamp'))
      next
    }
    rowtemp2 = rowtemp
    yr = substr(as.character(dataread[y,1]),1,4)
    pathwriteyr = paste(pathwrite,yr,sep="/")
    checkdir(pathwriteyr)
    mon = substr(as.character(dataread[y,1]),1,7)
    pathwritemon = paste(pathwriteyr,mon,sep="/")
    checkdir(pathwritemon)
		{
		if(temp==1)
		  temp2 = 'Solar'
		else if(temp == 2)
		  temp2 = 'Loads'
    }
		pathwritefinal = paste(pathwritemon,temp2,sep="/")
    checkdir(pathwritefinal)
    mtno = temp
    day = substr(as.character(dataread[y,1]),1,10)
    filename = paste(stnname,temp,"] ",day,".txt",sep="")
    pathtowrite = paste(pathwritefinal,filename,sep="/")
    rowtemp2[idxvals] = dataread[y,]
		{
    if(!file.exists(pathtowrite))
    {
      df = data.frame(rowtemp2,stringsAsFactors = F)
      colnames(df) = colnames
      if(idxts != 1)
        {
          df[idxts,] = rowtemp2
          df[1,] = rowtemp
        }
			FIREERRATA <<- c(1,1)
    }
    else
    {
      df = read.table(pathtowrite,header =T,sep = "\t",stringsAsFactors=F)
      df[idxts,] = rowtemp2
    }
  }
	if(erratacheck != 0)
	{
	pass = c(as.character(df[idxts,1]),as.character(df[idxts,9]))
	checkErrata(pass,idxts,mtno)
	}
	df = df[complete.cases(df[,1]),]
	tdx = as.POSIXct(strptime(as.character(df[,1]), "%Y-%m-%d %H:%M:%S"))
	df = df[order(tdx),]
  write.table(df,file = pathtowrite,row.names = F,col.names = T,sep = "\t",append = F)
	}
  recordTimeMaster("KH-002X","Gen1",as.character(df[nrow(df),1]))
}

path = '/home/admin/Data/Episcript-CEC/KH002 Dump'
pathwrite = '/home/admin/Dropbox/Gen 1 Data/[KH-002X]'
pathdatelastread = '/home/admin/Start'
checkdir(pathdatelastread)
pathdatelastread = paste(pathdatelastread,'KH002.txt',sep="/")
days = dir(path)
days = days[grepl("csv",days)]
lastrecordeddate = c('','')
idxtostart = c(1,1)
{
	if(!file.exists(pathdatelastread))
	{
		print('Last date read file not found so cleansing system from start')
		system('rm -R "/home/admin/Dropbox/Gen 1 Data/[KH-002X]"')
	}
	else
	{
		lastrecordeddate = readLines(pathdatelastread)
		lastrecordeddate = c(lastrecordeddate[1],lastrecordeddate[2])
		idxtostart[1] = match(lastrecordeddate[1],days)
		idxtostart[2] = match(lastrecordeddate[2],days)
		print(paste('Date read is',lastrecordeddate[1],'and idxtostart is',idxtostart[1]))
		print(paste('Date read is',lastrecordeddate[2],'and idxtostart is',idxtostart[2]))
	}
}
checkdir(pathwrite)
colnames = colnames(read.csv(paste(path,days[2],sep="/"),header = T, skip=6,stringsAsFactors=T))
colnames = colnames[-c(1,2)]
rowtemp = rep(NA,(length(colnames)))
x=1
stnname =  "[KH-002X-M"
days1 = days[grepl('1_1',days)]
days2 = days[grepl('2_2',days)]
{
	if(idxtostart[1] < length(days1))
	{
		print(paste('idxtostart[1] is',idxtostart[1],'while length of days1 is',length(days1)))
		for(x in idxtostart[1] : length(days1))
		{
 		 stitchFile(path,days1[x],pathwrite,0)
		}
		lastrecordeddate[1] = as.character(days1[x])
		print('Meter 1 DONE')
	}
	if(idxtostart[2] < length(days2))
	{
		print(paste('idxtostart[2] is',idxtostart[2],'while length of days2 is',length(days2)))
		for(x in idxtostart[2] : length(days2))
		{
 		 stitchFile(path,days2[x],pathwrite,0)
		}
		lastrecordeddate[2] = as.character(days2[x])
		print('Meter 2 Done')
	}
	
	else if( (!(idxtostart[1] < length(days[1]))) && (!(idxtostart[2] < length(days[2]))))
	{
		print('No historical backlog')
	}
}

print('_________________________________________')
print('Historical Calculations done')
print('_________________________________________')
reprint = 1
print(lastrecordeddate)
write(lastrecordeddate,pathdatelastread)
print('File Written')
while(1)
{
  print('Checking FTP for new data')
  filefetch = dumpftp(days,path)
	if(filefetch == 0)
	{
		Sys.sleep(600)
		next
	}
	print('FTP Check complete')
	daysnew = dir(path)
	daysnew = daysnew[grepl("csv",daysnew)]
	print('Done match')
	if(length(daysnew) == length(days))
	{
	print('condn true')
	  if(reprint == 1)
		{
			reprint = 0
			print('Waiting for new file dump')
		}
		print('sleeping')
		Sys.sleep(600)
		next
	}
	reprint = 1
	match = match(days,daysnew)
	days = daysnew
	daysnew = daysnew[-match]
	print('check done')
	for(x in 1 : length(daysnew))
	{
	  print(paste('Processing',daysnew[x]))
		stitchFile(path,daysnew[x],pathwrite,1)
		print(paste('Done',daysnew[x]))
	}
	daysnew1 = daysnew[grepl('1_1',daysnew)]
	daysnew2 = daysnew[grepl('2_2',daysnew)]
	if(length(daysnew1) < 1)
		daysnew1 = lastrecordeddate[1]
	if(length(daysnew2) < 1)
		daysnew2 = lastrecordeddate[2]
	write(c(as.character(daysnew1[length(daysnew1)]),as.character(daysnew2[length(daysnew2)])),pathdatelastread)
	lastrecordeddate[1] = as.character(daysnew1[length(daysnew1)])
	lastrecordeddate[2] = as.character(daysnew2[length(daysnew2)])
gc()
}
print('Out of loop')
sink()
