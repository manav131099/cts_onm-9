import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import pyodbc
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patches as mpatch
import matplotlib.dates as mdates
from scipy import stats
from matplotlib.ticker import MaxNLocator

path='/home/admin/Dropbox/Gen 1 Data/[IN-069L]/'
path_write='/home/admin/Graphs/'

capacity=12489.8
date=sys.argv[1]

def get_irr(date):
    df_pyr=pd.read_csv(path+date[0:4]+'/'+date[0:7]+'/WMS_1/'+"[IN-069L]-WMS1-"+date+'.txt',sep='\t')
    df_si=pd.read_csv(path+date[0:4]+'/'+date[0:7]+'/WMS_2/'+"[IN-069L]-WMS2-"+date+'.txt',sep='\t')
    df_pyr=df_pyr[['ts','OTI_avg']]
    df_si=df_si[['ts','POAI_avg']]
    df_merge=pd.merge(df_pyr,df_si, on=['ts'],sort=False,how='left')
    ghi_si=df_merge['POAI_avg'].sum()/12000
    ghi_pyr=df_merge['OTI_avg'].sum()/12000
    da_pyr=len(df_pyr.loc[:,'OTI_avg'].dropna())/2.88
    da_si=len(df_si.loc[:,'POAI_avg'].dropna())/1.64
    eac=0
    try:
        for i in range(3):
            df_1=pd.read_csv(path+date[0:4]+'/'+date[0:7]+'/MFM_'+str(i+1)+'/'+"[IN-069L]-MFM"+str(i+1)+"-"+date+'.txt',sep='\t')
            eac_1=(df_1['TotWhExp_max'].dropna().tail(1).values[0]-df_1['TotWhExp_max'].dropna().head(1).values[0])/1000
            eac=eac+eac_1
        pr_si=((eac/capacity)/ghi_si)*100
        pr_pyr=((eac/capacity)/ghi_pyr)*100
        df_final = pd.DataFrame({'Date':[date],'GHI_PYR':[ghi_pyr],'GHI_SI':[ghi_si],'PR_SI':[pr_si],'PR_PYR':[pr_pyr],'DA_SI':[da_si],'DA_PYR':[da_pyr]},columns =['Date','GHI_SI','GHI_PYR','PR_SI','PR_PYR','DA_SI','DA_PYR'])
    except:
        return pd.DataFrame({'Date':[date]},columns =['Date','GHI_SI','GHI_PYR','PR_SI','PR_PYR','DA_SI','DA_PYR'])
    return df_final

df=pd.DataFrame()

for i in sorted(os.listdir(path)):
    for j in sorted(os.listdir(path+'/'+i)):
        for k in sorted(os.listdir(path+'/'+i+'/'+j)):
            if('MFM_1' in k):
                for l in sorted(os.listdir(path+'/'+i+'/'+j+'/'+k)):
                    df_temp=(get_irr(l[-14:][:-4]))
                    df=df.append(df_temp)
df.to_csv(path_write+"Graph_Extract/IN-069/[IN-069] Graph "+date+" - Pyranometer vs Silicon Sensor.txt",sep='\t',index=False)

df_pr=df[['Date','PR_SI','PR_PYR','DA_PYR','DA_SI','GHI_SI','GHI_PYR']]
df_pr=df_pr[df_pr['Date']>'2020-04-31']
df_pr=df_pr[df_pr['Date']<=date]
df_pr=df_pr.loc[((df_pr['DA_PYR']>95) & (df_pr['DA_SI']>95)),:]
no_points=len(df_pr)
avg_si=df_pr['PR_SI'].mean()
avg_pyr=df_pr['PR_PYR'].mean()
df_pr['Date']=pd.to_datetime(df_pr['Date'])

plt.rcParams.update({'font.size': 28})

#Plot 1
fig = plt.figure(num=None, figsize=(55  , 30))
ax = fig.add_subplot(111)
a=ax.scatter(df_pr['Date'].tolist(), df_pr['PR_PYR'],color='dodgerblue',s=110)
b=ax.scatter(df_pr['Date'].tolist(), df_pr['PR_SI'],color='darkorange',s=110)
myFmt = mdates.DateFormatter('%Y-%m-%d')
ax.xaxis.set_major_formatter(myFmt)
ax.set_ylabel('Performance Ratio [%]', fontsize=32)
plt.legend((a,b),('PR Calculated using Pyronameter', 'PR Calculated using Silicon Sensor'),scatterpoints=1,loc=(0.02,.9),ncol=1,fontsize=25)
ax.set_ylim([0,100])
ax.annotate('No. of Points: '+str(no_points)+' days\n\nAverage PR using Pyranometer [%]: '+str(round(avg_pyr,1))+'\n\nAverage PR using Silicon Sensor [%]: '+str(round(avg_si,1)), (.7, .1),xytext=(4, -4),xycoords='axes fraction',textcoords='offset points',fontweight='bold',color='black',ha='left', bbox=dict(boxstyle="square", fc="none", ec="black",lw=5), va='bottom',size=25)
fig.savefig(path_write+"Graph_Output/IN-069/[IN-069] Graph (Beta) "+date+" - Pyranometer PR vs Silicon Sensor PR.pdf", bbox_inches='tight')

#Plot 2
fig2 = plt.figure(num=None, figsize=(55  , 30))
ax2 = fig2.add_subplot(111)
a=ax2.scatter(df_pr['GHI_PYR'].tolist(), df_pr['PR_PYR'],color='dodgerblue',s=110)
ax2.set_ylabel('Performance Ratio [%]', fontsize=32)
ax2.set_xlabel('Global Horizontal Irradiance [W/m$^2$]', fontsize=32)
ax2.set_ylim([0,100])
ax2.annotate('No. of Points: '+str(no_points)+' days\n\nAverage PR using Pyranometer [%]: '+str(round(avg_pyr,1)), (.68, .1),xytext=(4, -4),xycoords='axes fraction',textcoords='offset points',fontweight='bold',color='black',ha='left', bbox=dict(boxstyle="square", fc="none", ec="black",lw=5), va='bottom',size=25)
plt.legend([a],['PR Calculated using Pyronameter'],scatterpoints=1,loc=(0.02,.9),ncol=1,fontsize=25)
plt.gca().xaxis.set_major_locator(MaxNLocator(prune='lower'))
fig2.savefig(path_write+"Graph_Output/IN-069/[IN-069] Graph (Beta) "+date+" - PR vs GHI Pyranometer.pdf", bbox_inches='tight')

#Plot 3
fig3 = plt.figure(num=None, figsize=(55  , 30))
ax3 = fig3.add_subplot(111)
a=ax3.scatter(df_pr['GHI_SI'].tolist(), df_pr['PR_SI'],color='darkorange',s=110)
ax3.set_ylabel('Performance Ratio [%]', fontsize=32)
ax3.set_xlabel('Global Horizontal Irradiance [W/m$^2$]', fontsize=32)
ax3.set_ylim([0,100])
plt.legend([a],['PR Calculated using Silicon Sensor'],scatterpoints=1,loc=(0.02,.9),ncol=1,fontsize=25)
plt.gca().xaxis.set_major_locator(MaxNLocator(prune='lower'))
ax3.annotate('No. of Points: '+str(no_points)+'\n\nAverage PR using Silicon Sensor [%]: '+str(round(avg_si,1)), (.68, .1),xytext=(4, -4),xycoords='axes fraction',textcoords='offset points',fontweight='bold',color='black',ha='left', bbox=dict(boxstyle="square", fc="none", ec="black",lw=5), va='bottom',size=25)
fig3.savefig(path_write+"Graph_Output/IN-069/[IN-069] Graph (Beta) "+date+" - PR vs GHI Silicon Sensor.pdf", bbox_inches='tight')
