rm(list=ls(all =TRUE))

library(ggplot2)
library(reshape)
library(lubridate)
library(devtools)

pathWrite <- "C:/Users/16030503/Desktop/Cleantech Solar/Wall Graph/Plot Wall Graph/"
result <- read.csv("C:/Users/16030503/Desktop/Cleantech Solar/Wall Graph/Data Extracted/SG-004S_PR_summary.csv",stringsAsFactors = F)

rownames(result) <- NULL
result <- data.frame(result)
result$Date <- as.POSIXct(result$Date, format = "%Y-%m-%d")

PR_MA_avg = mean(result$PR.MA,na.rm=TRUE)
PR_MB_avg = mean(result$PR.MB,na.rm=TRUE)


PR_MA_std = sd(result$PR.MA,na.rm=TRUE)
PR_MB_std = sd(result$PR.MB,na.rm=TRUE)


PR_MA_sigma2POS = PR_MA_avg + (2*PR_MA_std)
PR_MA_sigma2NEG = PR_MA_avg - (2*PR_MA_std)
PR_MB_sigma2POS = PR_MB_avg + (2*PR_MA_std)
PR_MB_sigma2NEG = PR_MB_avg - (2*PR_MA_std)

No_of_points <- nrow(result)

###########################################################################################################################################################
# Meter A without filter 

m <- lm(result$PR.MA ~ result$No..of.Days)
a <- signif(coef(m)[1], digits = 2)
b <- signif(coef(m)[2], digits = 2)
equation <- paste("y = ",a," ", b," x", sep="")

PRGraph <- ggplot(data=result,aes(x=No..of.Days,y=PR.MA)) + geom_point() +
  stat_smooth(method = lm, se = FALSE, show.legend = TRUE) +
  #scale_y_continuous(limits = c(50, 95), expand= c(0,0), breaks=seq(50,100,10)) +
  scale_x_continuous(limits = c(0, 650), expand= c(0,0),breaks=seq(0,650,100)) +
  ggtitle(expression("[SG-004S] Meter A Lifetime Performance Ratio (without 2"*sigma~"filter)"), subtitle = paste0("From ",result$Date[1]," to ",result$Date[nrow(result)])) +
  theme(plot.title = element_text(face = "bold",size = 12,lineheight = 0.7,hjust = 0.5, margin = margin(0,0,7,0)),
        plot.subtitle = element_text(face = "bold",size = 12,lineheight = 0.9,hjust = 0.5)) +
  ylab("Performance Ratio AC [%]") + 
  xlab("# of Days") +
  theme(axis.text.x = element_text(size=11))+
  theme(axis.text.y = element_text(size=11))+
  theme(axis.title.y = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))+
  theme(axis.title.x = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))+
  geom_hline(yintercept=PR_MA_sigma2POS, linetype="dashed") +
  geom_hline(yintercept=PR_MA_sigma2NEG, linetype="dashed") + 
  annotate("text",label = expression("+2"*sigma~"[89.5%]"),size = 3,  #update the value(%) according to PR_MA_sigma2POS
           x = 50, y= PR_MA_sigma2POS+1.5,fontface =1,hjust = 0) +
  annotate("text",label = expression("-2"*sigma~"[75.5%]"),size = 3,   #update the value(%) according to PR_MA_sigma2NEG
           x = 50, y= PR_MA_sigma2NEG-1.5,fontface =1,hjust = 0) +
  annotate("text",label = paste0("Avg PR = ",sprintf("%0.1f", PR_MA_avg)," %"),size = 3,
           x = 400, y= 64,fontface =1,hjust = 0) +
  annotate("text",label = paste0("Degradation = ", sprintf("%0.1f", b*365) ," % p.a"),size = 3,
           x = 400, y= 62,fontface =1,hjust = 0) +
  annotate("text",label = paste0(equation),size = 3,
           x = 400, y= 95,fontface =1,hjust = 0) +
  annotate("text",label = paste0("Start PR = ",sprintf("%0.1f", a), " %"),size = 3,
           x = 400, y= 93,fontface =1,hjust = 0) +
  annotate("text",label = paste0("Current PR = ",sprintf("%0.1f", a+No_of_points*b)," %"),size = 3,
           x = 400, y= 91,fontface =1,hjust = 0) +
  theme(plot.margin = unit(c(0.5,0.5,0.2,0.2),"cm"))  #top, right, bottom, left

ggsave(PRGraph,filename = paste0(pathWrite,"SG-004S_A_lifetime_PR_Without_Filter.pdf"), width = 7.92, height = 5)

###########################################################################################################################################################
# Meter B without filter 

m <- lm(result$PR.MB ~ result$No..of.Days)
a <- signif(coef(m)[1], digits = 2)
b <- signif(coef(m)[2], digits = 2)
equation <- paste("y = ",a," ", b," x", sep="")

PRGraph <- ggplot(data=result,aes(x=No..of.Days,y=PR.MB)) + geom_point() +
  stat_smooth(method = lm, se = FALSE, show.legend = TRUE) +
  #scale_y_continuous(limits = c(50, 95), expand= c(0,0), breaks=seq(50,100,10)) +
  scale_x_continuous(limits = c(0, 650), expand= c(0,0),breaks=seq(0,650,100)) +
  ggtitle(expression("[SG-004S] Meter B Lifetime Performance Ratio (without 2"*sigma~"filter)"), subtitle = paste0("From ",result$Date[1]," to ",result$Date[nrow(result)])) +
  theme(plot.title = element_text(face = "bold",size = 12,lineheight = 0.7,hjust = 0.5, margin = margin(0,0,7,0)),
        plot.subtitle = element_text(face = "bold",size = 12,lineheight = 0.9,hjust = 0.5)) +
  ylab("Performance Ratio AC [%]") + 
  xlab("# of Days") +
  theme(axis.text.x = element_text(size=11))+
  theme(axis.text.y = element_text(size=11))+
  theme(axis.title.y = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))+
  theme(axis.title.x = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))+
  geom_hline(yintercept=PR_MB_sigma2POS, linetype="dashed") +
  geom_hline(yintercept=PR_MB_sigma2NEG, linetype="dashed") + 
  annotate("text",label = expression("+2"*sigma~"[88.8%]"),size = 3,  #update the value(%) according to PR_MB_sigma2POS
           x = 160, y= PR_MB_sigma2POS+1.5,fontface =1,hjust = 0) +
  annotate("text",label = expression("-2"*sigma~"[74.8%]"),size = 3,   #update the value(%) according to PR_MB_sigma2NEG
           x = 160, y= PR_MB_sigma2NEG-1.5,fontface =1,hjust = 0) +
  annotate("text",label = paste0("Avg PR = ",sprintf("%0.1f", PR_MB_avg)," %"),size = 3,
           x = 400, y= 64,fontface =1,hjust = 0) +
  annotate("text",label = paste0("Degradation = ", sprintf("%0.1f", b*365) ," % p.a"),size = 3,
           x = 400, y= 62,fontface =1,hjust = 0) +
  annotate("text",label = paste0(equation),size = 3,
           x = 400, y= 94,fontface =1,hjust = 0) +
  annotate("text",label = paste0("Start PR = ",sprintf("%0.1f", a), " %"),size = 3,
           x = 400, y= 92,fontface =1,hjust = 0) +
  annotate("text",label = paste0("Current PR = ",sprintf("%0.1f", a+No_of_points*b)," %"),size = 3,
           x = 400, y= 90,fontface =1,hjust = 0) +
  theme(plot.margin = unit(c(0.5,0.5,0.2,0.2),"cm"))  #top, right, bottom, left

ggsave(PRGraph,filename = paste0(pathWrite,"SG-004S_B_lifetime_PR_Without_Filter.pdf"), width = 7.92, height = 5)

###########################################################################################################################################################
# filtering 2 sigma

exclude_index_A <- c()
exclude_index_B <- c()

count_A <- 0
count_B <- 0

 for(i in 1:nrow(result)){
   #METER A
   if(isTRUE (result[i,3] < PR_MA_sigma2NEG)){  # eliminate PR below -2Sigma
     exclude_index_A <- c(exclude_index_A,i)
     count_A <- count_A + 1
   }

   if(isTRUE (result[i,3] > PR_MA_sigma2POS)){   # eliminate PR above +2Sigma
     exclude_index_A <- c(exclude_index_A,i)
     count_A <- count_A + 1
   }
   
   #METER B
   if(isTRUE (result[i,4] < PR_MB_sigma2NEG)){  # eliminate PR below -2Sigma
     exclude_index_B <- c(exclude_index_B,i)
     count_B <- count_B + 1
   }
   
   if(isTRUE (result[i,4] > PR_MB_sigma2POS)){   # eliminate PR above +2Sigma
     exclude_index_B <- c(exclude_index_B,i)
     count_B <- count_B + 1
   }
   
   
 }
result[exclude_index_A,3] <- NA
result[exclude_index_B,4] <- NA


#mdata <- melt(result, id=c("Meter.Reference","Date","No..of.Days"))

# PR_MA_eliminated = as.numeric(length(which(is.na(result$PR.MA))))
# PR_MB_eliminated = as.numeric(length(which(is.na(result$PR.MB))))
# PR_MC_eliminated = as.numeric(length(which(is.na(result$PR.MC))))

###########################################################################################################################################################
# Meter A with filter 

 
 m <- lm(result$PR.MA ~ result$No..of.Days)
 a <- signif(coef(m)[1], digits = 2)
 b <- signif(coef(m)[2], digits = 2)
 equation <- paste("y = ",a," ", b," x", sep="")
 
PRGraph <- ggplot(data=result,aes(x=No..of.Days,y=PR.MA)) + geom_point(na.rm=TRUE) +
    stat_smooth(method = lm, se = FALSE, show.legend = TRUE,na.rm=TRUE) +
    geom_hline(yintercept=PR_MA_sigma2POS, linetype="dashed") +
    geom_hline(yintercept=PR_MA_sigma2NEG, linetype="dashed") +
    scale_y_continuous(limits = c(70, 94), expand= c(0,0), breaks=seq(70,94,10)) +
    scale_x_continuous(limits = c(0, 650), expand= c(0,0),breaks=seq(0,650,100)) +
    ggtitle(paste("[SG-004S] Meter A Lifetime Performance Ratio"), subtitle = paste0("From ",result$Date[1]," to ",result$Date[nrow(result)])) +
    theme(plot.title = element_text(face = "bold",size = 12,lineheight = 0.7,hjust = 0.5, margin = margin(0,0,7,0)),
          plot.subtitle = element_text(face = "bold",size = 12,lineheight = 0.9,hjust = 0.5)) +
    ylab("Performance Ratio AC [%]") +
    xlab("# of Days") +
    theme(axis.text.x = element_text(size=11))+
    theme(axis.text.y = element_text(size=11))+
    theme(axis.title.y = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))+
    theme(axis.title.x = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))+
    geom_hline(yintercept=PR_MA_sigma2POS, linetype="dashed") +
    geom_hline(yintercept=PR_MA_sigma2NEG, linetype="dashed") +
    annotate("text",label = expression("+2"*sigma~"[89.5%]"),size = 3,  #update the value(%) according to PR_MA_sigma2POS
              x = 100, y= PR_MA_sigma2POS+1.5,fontface =1,hjust = 0) +
    annotate("text",label = expression("-2"*sigma~"[75.5%]"),size = 3,   #update the value(%) according to PR_MA_sigma2NEG
             x = 100, y= PR_MA_sigma2NEG-1.5,fontface =1,hjust = 0) +
    annotate("text",label = paste0("Avg PR = ",sprintf("%0.1f", PR_MA_avg)," %"),size = 3,
             x = 400, y= 75,fontface =1,hjust = 0) +
    annotate("text",label = paste0("Degradation = ", sprintf("%0.1f", b*365) ," % p.a"),size = 3,
             x = 400, y= 74,fontface =1,hjust = 0) +
    annotate("text",label = paste0("Number of Point availble = ", nrow(result)-count_A),size = 3,
             x = 400, y= 73,fontface =1,hjust = 0) +
    annotate("text",label = expression("Points eliminated (2"*sigma~"filter) = 16"),size = 3, #update the value(%) according to count_A
             x = 400, y= 72,fontface =1,hjust = 0) +
    annotate("text",label = paste0("% of points eliminated = ",round(count_A/No_of_points*100,1)," %"),size = 3,
             x = 400, y= 71,fontface =1,hjust = 0) +
    annotate("text",label = paste0(equation),size = 3,
            x = 400, y= 93,fontface =1,hjust = 0) +
    annotate("text",label = paste0("Start PR = ",sprintf("%0.1f", a), " %"),size = 3,
             x = 400, y= 92,fontface =1,hjust = 0) +
    annotate("text",label = paste0("Current PR = ",sprintf("%0.1f", a+No_of_points*b)," %"),size = 3,
             x = 400, y= 91,fontface =1,hjust = 0) +
    theme(plot.margin = unit(c(0.5,0.5,0.2,0.2),"cm"))  #top, right, bottom, left
  

ggsave(PRGraph,filename = paste0(pathWrite,"SG-004S_A_lifetime_PR.pdf"), width = 7.92, height = 5) 
 

###########################################################################################################################################################
# Meter B with filter 

 
 m <- lm(result$PR.MB ~ result$No..of.Days)
 a <- signif(coef(m)[1], digits = 2)
 b <- signif(coef(m)[2], digits = 2)
 equation <- paste("y = ",a," ", b," x", sep="")
 
 PRGraph <- ggplot(data=result,aes(x=No..of.Days,y=PR.MB)) + geom_point(na.rm=TRUE) +
   stat_smooth(method = lm, se = FALSE, show.legend = TRUE,na.rm=TRUE) +
   geom_hline(yintercept=PR_MB_sigma2POS, linetype="dashed") +
   geom_hline(yintercept=PR_MB_sigma2NEG, linetype="dashed") +
   scale_y_continuous(limits = c(70, 92), expand= c(0,1), breaks=seq(70,92,10)) +
   scale_x_continuous(limits = c(0, 650), expand= c(0,0),breaks=seq(0,650,100)) +
   ggtitle(paste("[SG-004S] Meter B Lifetime Performance Ratio"), subtitle = paste0("From ",result$Date[1]," to ",result$Date[nrow(result)])) +
   theme(plot.title = element_text(face = "bold",size = 12,lineheight = 0.7,hjust = 0.5, margin = margin(0,0,7,0)),
         plot.subtitle = element_text(face = "bold",size = 12,lineheight = 0.9,hjust = 0.5)) +
   ylab("Performance Ratio AC [%]") +
   xlab("# of Days") +
   theme(axis.text.x = element_text(size=11))+
   theme(axis.text.y = element_text(size=11))+
   theme(axis.title.y = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))+
   theme(axis.title.x = element_text(size = 11, face = "bold",margin = margin(0,2,0,0)))+
   geom_hline(yintercept=PR_MB_sigma2POS, linetype="dashed") +
   geom_hline(yintercept=PR_MB_sigma2NEG, linetype="dashed") +
   annotate("text",label = expression("+2"*sigma~"[88.8%]"),size = 3,  #update the value(%) according to PR_MB_sigma2POS
            x = 100, y= PR_MB_sigma2POS+1.5,fontface =1,hjust = 0) +
   annotate("text",label = expression("-2"*sigma~"[74.8%]"),size = 3,   #update the value(%) according to PR_MB_sigma2NEG
            x = 100, y= PR_MB_sigma2NEG-1.5,fontface =1,hjust = 0) +
   annotate("text",label = paste0("Avg PR = ",sprintf("%0.1f", PR_MB_avg)," %"),size = 3,
            x = 400, y= 74,fontface =1,hjust = 0) +
   annotate("text",label = paste0("Degradation = ", sprintf("%0.1f", b*365) ," % p.a"),size = 3,
            x = 400, y= 73,fontface =1,hjust = 0) +
   annotate("text",label = paste0("Number of Point availble = ", nrow(result)-count_B),size = 3,
            x = 400, y= 72,fontface =1,hjust = 0) +
   annotate("text",label = expression("Points eliminated (2"*sigma~"filter) = 27"),size = 3, #update the value according to count_B
            x = 400, y= 71,fontface =1,hjust = 0) +
   annotate("text",label = paste0("% of points eliminated = ",round(count_B/No_of_points*100,1)," %"),size = 3,
            x = 400, y= 70,fontface =1,hjust = 0) +
   annotate("text",label = paste0(equation),size = 3,
            x = 400, y= 92,fontface =1,hjust = 0) +
   annotate("text",label = paste0("Start PR = ",sprintf("%0.1f", a), " %"),size = 3,
            x = 400, y= 91,fontface =1,hjust = 0) +
   annotate("text",label = paste0("Current PR = ",sprintf("%0.1f", a+No_of_points*b)," %"),size = 3,
            x = 400, y= 90,fontface =1,hjust = 0) +
   theme(plot.margin = unit(c(0.5,0.5,0.2,0.2),"cm"))  #top, right, bottom, left
 
ggsave(PRGraph,filename = paste0(pathWrite,"SG-004S_B_lifetime_PR.pdf"), width = 7.92, height = 5) 
