import os
import re
import pyodbc
import csv,time
import datetime
import collections
import os
import time
import pytz

str_val = [0,13,20,27,34,41,48,55,62,69,76,83,90,97,104,111,118,125]

server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 13 for SQL Server}'

##global start_tstamp
##global start_tstamp_update

tz = pytz.timezone('Asia/Kolkata')
last_date = datetime.datetime.now(tz).date()

##read_path = 'F:/del/FlexiMC_Data/Azure_Fourth_Gen/[IN-023C]/[IN-023C]-lifetime.txt' ###local
read_path = '/home/admin/Dropbox/FlexiMC_Data/Azure_Fourth_Gen/[IN-023C]/[IN-023C]-latest.txt' ###server


#Function to check for new day
def determine_date():
    global last_date    
    tz = pytz.timezone('Asia/Kolkata')
    today_date = datetime.datetime.now(tz).date()
    if today_date != last_date:
        last_date = today_date
        print('New day, Deleting old records')
        delete_records(last_date)

#Function delete old records
def delete_records(last_date):
    last_date = last_date + datetime.timedelta(days=-35)
    last_date = last_date.strftime("%Y-%m-%d %H:%M:%S")

    while True:
        cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
        try:
            cursor = cnxn.cursor()
            with cursor.execute("DELETE FROM [Cleantech Meter Readings].dbo.[IN-023C-ALL] WHERE [Tstamp]<?",last_date):
                print('Detetion successful - All records deleted before '+last_date)
            cnxn.commit()
            cursor.close()
            cnxn.close()
            break
            
        except Exception as e:
                err_num = e[0]
                print ('ERROR:', err_num,e[1])
                cursor.close()
                cnxn.close()
                if err_num == '08S01':
                    counter = counter + 1
                    print ('Sleeping for 5')
                    time.sleep(300)
                    if counter > 10:
                        exit('Persistent connection error')                    
                    print ('%%%Re-trying connection%%%')             
                else:
                    break
        
    

#Function to read start timestamp
def read_timestamp():
    global start_tstamp
##    file = open('F:/Flexi_final/[IN-023C]/IN023CAA.txt','r') ###local
    file = open('/home/admin/Start/IN023CAA.txt','r') ###server
    time_read = file.read(19)
    start_tstamp = datetime.datetime.strptime(time_read, "%Y-%m-%d %H:%M:%S")

#Function to update start file 
def update_start_file(curr_tstamp):
##    file = open('F:/Flexi_final/[IN-023C]/IN023CAA.txt','w') ###local
    file = open('/home/admin/Start/IN023CAA.txt','w') ###server
    file.write(str(curr_tstamp))
    file.close()

def insert(fields,curr_tstamp):
 
    counter = 0
    while True:            
        try:
            cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
            cursor = cnxn.cursor()
            update_start_file(fields[0])
            global start_tstamp_update
            start_tstamp_update = curr_tstamp
            
            with cursor.execute("INSERT INTO [Cleantech Meter Readings].dbo.[IN-023C-ALL] ([Tstamp],[WMS.GTI],[WMS.Tamb],[WMS.Tmod],[WMS.TambSH],[WMS.TmodSH],[MFM.Eac1],[MFM.Eac2],[MFM.Yld1],[MFM.Yld2],[MFM.PR1],[MFM.PR2],[MFM.LastRead],[MFM.LastTime],[Inverter_1.Eac1],[Inverter_1.EacAC],[Inverter_1.EacDC],[Inverter_1.Yld1],[Inverter_1.Yld2],[Inverter_1.LastRead],[Inverter_1.LastTime],[Inverter_2.Eac1],[Inverter_2.EacAC],[Inverter_2.EacDC],[Inverter_2.Yld1],[Inverter_2.Yld2],[Inverter_2.LastRead],[Inverter_2.LastTime],[Inverter_3.Eac1],[Inverter_3.EacAC],[Inverter_3.EacDC],[Inverter_3.Yld1],[Inverter_3.Yld2],[Inverter_3.LastRead],[Inverter_3.LastTime],[Inverter_4.Eac1],[Inverter_4.EacAC],[Inverter_4.EacDC],[Inverter_4.Yld1],[Inverter_4.Yld2],[Inverter_4.LastRead],[Inverter_4.LastTime],[Inverter_5.Eac1],[Inverter_5.EacAC],[Inverter_5.EacDC],[Inverter_5.Yld1],[Inverter_5.Yld2],[Inverter_5.LastRead],[Inverter_5.LastTime],[Inverter_6.Eac1],[Inverter_6.EacAC],[Inverter_6.EacDC],[Inverter_6.Yld1],[Inverter_6.Yld2],[Inverter_6.LastRead],[Inverter_6.LastTime],[Inverter_7.Eac1],[Inverter_7.EacAC],[Inverter_7.EacDC],[Inverter_7.Yld1],[Inverter_7.Yld2],[Inverter_7.LastRead],[Inverter_7.LastTime],[Inverter_8.Eac1],[Inverter_8.EacAC],[Inverter_8.EacDC],[Inverter_8.Yld1],[Inverter_8.Yld2],[Inverter_8.LastRead],[Inverter_8.LastTime],[Inverter_9.Eac1],[Inverter_9.EacAC],[Inverter_9.EacDC],[Inverter_9.Yld1],[Inverter_9.Yld2],[Inverter_9.LastRead],[Inverter_9.LastTime],[Inverter_10.Eac1],[Inverter_10.EacAC],[Inverter_10.EacDC],[Inverter_10.Yld1],[Inverter_10.Yld2],[Inverter_10.LastRead],[Inverter_10.LastTime],[Inverter_11.Eac1],[Inverter_11.EacAC],[Inverter_11.EacDC],[Inverter_11.Yld1],[Inverter_11.Yld2],[Inverter_11.LastRead],[Inverter_11.LastTime],[Inverter_12.Eac1],[Inverter_12.EacAC],[Inverter_12.EacDC],[Inverter_12.Yld1],[Inverter_12.Yld2],[Inverter_12.LastRead],[Inverter_12.LastTime],[Inverter_13.Eac1],[Inverter_13.EacAC],[Inverter_13.EacDC],[Inverter_13.Yld1],[Inverter_13.Yld2],[Inverter_13.LastRead],[Inverter_13.LastTime],[Inverter_14.Eac1],[Inverter_14.EacAC],[Inverter_14.EacDC],[Inverter_14.Yld1],[Inverter_14.Yld2],[Inverter_14.LastRead],[Inverter_14.LastTime],[Inverter_15.Eac1],[Inverter_15.EacAC],[Inverter_15.EacDC],[Inverter_15.Yld1],[Inverter_15.Yld2],[Inverter_15.LastRead],[Inverter_15.LastTime],[Inverter_16.Eac1],[Inverter_16.EacAC],[Inverter_16.EacDC],[Inverter_16.Yld1],[Inverter_16.Yld2],[Inverter_16.LastRead],[Inverter_16.LastTime]) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",fields): 
                print('Successfuly Inserted!' + fields[0])                
            
            cnxn.commit()
            cursor.close()
            cnxn.close()
            break
        
        except Exception as e:
            err_num = e[0]
            print ('ERROR:', err_num,e[1], fields[0])
            cursor.close()
            cnxn.close()
            if err_num == '08S01':
                counter = counter + 1
                print ('Sleeping for 5')
                time.sleep(300)
                if counter > 10:
                    exit('Persistent connection error')                    
                print ('%%%Re-trying connection%%%')             
            else:
                break




def convert_data_type(fields):
    for i in range(len(fields)):
        if i in str_val:
            continue
        if fields[i] == 'NA' or fields[i] == 'NA\n' :
            fields[i] = None
        else:
            fields[i] = float(fields[i])

    return fields
        





read_timestamp()
start_tstamp_update = start_tstamp
while True:

    determine_date()
       
    try:
        if not os.path.exists(read_path):
                raise Exception('Empty')        
    except Exception as e:
            print('No file found -',read_path)
            time.sleep(600)
            continue

    start_tstamp = start_tstamp_update
    with open(read_path, 'r') as read_file:
        first = 1
        for line in read_file:
            if first == 1:
                first = 0
                continue
            fields =[]
            fields = line.split("\t")
##            fields[0] = fields[0].replace('"', '').strip()
##            fields[6] = fields[6].replace('"', '').strip()
            for i in str_val:
                fields[i] = fields[i].replace('"','').strip()
    
            curr_tstamp = datetime.datetime.strptime(fields[0], "%Y-%m-%d %H:%M:%S")
            if curr_tstamp > start_tstamp:
                fields = convert_data_type(fields)
                insert(fields,curr_tstamp)
                
    time.sleep(1800)
