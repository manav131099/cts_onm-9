rm(list = ls())
errHandle = file('/home/admin/Logs/LogsIN018History.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

require('mailR')
source('/home/admin/CODE/IN018Digest/FTPIN018Dump.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
source('/home/admin/CODE/Misc/memManage.R')
FIREERRATA = c(1,1)
LTCUTOFF = 0.001
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

timetomins = function(x)
{
  hrs = unlist(strsplit(x,":"))
  mins = as.numeric(hrs[2])
  hrs = as.numeric(hrs[1]) * 60
  return((((hrs + mins))+1))
}
FIRETWILIONA = 0
checkErrata = function(row,ts)
{
	no =1
  if(ts < 481 || ts > 960 || no == 2)
	{return()}
	if(FIREERRATA[no] == 0)
	{
	  print(paste('Errata mail already sent for meter',no,'so no more mails'))
		return()
	}
	
	{
	if(!is.finite(as.numeric(row[2])))
	{
		FIRETWILIONA <<- FIRETWILIONA + 1
	}
	else if(as.numeric(row[2]) < LTCUTOFF)
		FIRETWILIONA <<- FIRETWILIONA + 1
	else
		FIRETWILIONA <<- 0
	}

	if((FIRETWILIONA > 10))
	#|| ((as.numeric(row[2]) < LTCUTOFF) && (is.finite(as.numeric(row[2])))))
	{
		row[2] = as.character(round(as.numeric(row[2]),2))
		FIREERRATA[no] <<- 0
		subj = paste('IN-018X down')
    body = 'Last recorded timestamp and reading\n'
		body = paste(body,'\n\nTimestamp:',as.character(row[1]))
		body = paste(body,'\nReal Power Tot kW reading:',as.character(row[2]))
		{
		if(!is.finite(as.numeric(row[2])))
		{
		body = paste(body,'\nMore than 10 continuous readings are NA')
		}
		else
		body = paste(body,'\nMore than 10 continuous readings are below threshold(',LTCUTOFF,')',sep="")
		}

		send.mail(from = 'operations@cleantechsolar.com',
							to = getRecipients("IN-018X","a"),
							subject = subj,
							body = body,
							smtp = list(host.name = "smtp.office365.com", port = 587, user.name = 'shravan.karthik@cleantechsolar.com',passwd = 'CTS&*(789', tls= TRUE),
							authenticate = TRUE,
							send = TRUE,
							debug = F)
		print(paste('Errata mail sent meter ',no))
		msgbody = paste(subj,"\n Timestamp:",as.character(row[1]),
		                "\n Real Pow Tot kW reading:",as.character(row[2]))
		command = paste('python /home/admin/CODE/Send_mail/twilio_alert.py',' "',msgbody,'"',' "IN-018X"',sep = "")
		system(command)
		print('Twilio message fired')
		recordTimeMaster("IN-018X","TwilioAlert",as.character(row[1]))
		FIRETWILIONA <<-0
	}
}

checkFormatDate = function(data)
{
	dataSplit = unlist(strsplit(data,"-"))
	seq1 = seq(from=1, to=length(dataSplit), by = 3)
	if(!is.finite(as.numeric(dataSplit[1])))
		return(NULL)
	print('Missmatch detected')
	data = paste(dataSplit[(seq1+1)],dataSplit[(seq1)],dataSplit[(seq1+2)],sep="-")
	return(data)
}

stitchFile = function(path,days,pathwrite,erratacheck)
{
x = 1
t = try(read.csv(paste(path,days[x],sep="/"),header = T, skip=6,stringsAsFactors=F),silent = T)
  if(length(t)==1)
        {
                print(paste("Skipping",days[x],"Due to err in file"))
                return()
        }
  dataread = t
        if(nrow(dataread) < 1)
                return()
  dataread = dataread[,-c(1,2)]
	updatedDate = checkFormatDate(as.character(dataread[,1]))
	if(!is.null(updatedDate))
		dataread[,1] = updatedDate
	tmstmps = as.character(dataread[,1])
	tmstmps = unlist(strsplit(tmstmps," "))
  allseqs = seq(from = 1, to = length(tmstmps),by=2)
	tmstmps2 = tmstmps[allseqs]
	tmstmps2 = unlist(strsplit(tmstmps2,"-"))
	allseqa = seq(from = 1, to = length(tmstmps2),by=3)
	tmstmps2 = paste(tmstmps2[(allseqa+2)],tmstmps2[(allseqa+1)],tmstmps2[(allseqa)],sep="-")
	tmstmps = paste(tmstmps2,tmstmps[(allseqs+1)])
	dataread[,1] = tmstmps
	currcolnames = colnames(dataread)
  idxvals = match(currcolnames,colnames)
  for(y in 1 : nrow(dataread))
  {
    idxts = timetomins(substr(as.character(dataread[y,1]),12,16))
    if(!is.finite(idxts))
    {
      print(paste('Skipping row',y,'as incorrect timestamp'))
      next
    }
    rowtemp2 = rowtemp
    yr = substr(as.character(dataread[y,1]),1,4)
    pathwriteyr = paste(pathwrite,yr,sep="/")
    checkdir(pathwriteyr)
    mon = substr(as.character(dataread[y,1]),1,7)
    pathwritemon = paste(pathwriteyr,mon,sep="/")
    checkdir(pathwritemon)
		pathwritefinal = pathwritemon
    checkdir(pathwritefinal)
    day = substr(as.character(dataread[y,1]),1,10)
    filename = paste(stnname,day,".txt",sep="")
    pathtowrite = paste(pathwritefinal,filename,sep="/")
    rowtemp2[idxvals] = dataread[y,]
		{
    if(!file.exists(pathtowrite))
    {
      df = data.frame(rowtemp2,stringsAsFactors = F)
      colnames(df) = colnames
      if(idxts != 1)
        {
          df[idxts,] = rowtemp2
          df[1,] = rowtemp
        }
			FIREERRATA <<- c(1,1)
			FIRETWILIONA <<-0
    }
    else
    {
      df = read.table(pathtowrite,header =T,sep = "\t",stringsAsFactors=F)
      df[idxts,] = rowtemp2
    }
  }
	if(erratacheck != 0)
	{
	pass = c(as.character(df[idxts,1]),as.character(df[idxts,15]))
	checkErrata(pass,idxts)
	}
	df = df[complete.cases(df[,1]),]
	tdx = as.POSIXct(strptime(as.character(df[,1]), "%Y-%m-%d %H:%M:%S"))
	df = df[order(tdx),]
  write.table(df,file = pathtowrite,row.names = F,col.names = T,sep = "\t",append = F)
  }
	recordTimeMaster("IN-018X","Gen1",as.character(df[nrow(df),1]))
}

path = '/home/admin/Data/Episcript-CEC/EGX IN-018 Dump'
checkdir(path)
pathwrite = '/home/admin/Dropbox/Gen 1 Data/[IN-018X]'
pathdatelastread = '/home/admin/Start'
checkdir(pathdatelastread)
pathdatelastread = paste(pathdatelastread,'IN-018.txt',sep="/")
days = dir(path)
days = days[grepl("csv",days)]
lastrecordeddate = c('')
idxtostart = c(1)
{
	if(!file.exists(pathdatelastread))
	{
		print('Last date read file not found so cleansing system from start')
		system('rm -R "/home/admin/Dropbox/Gen 1 Data/[IN-018X]"')
	}
	else
	{
		lastrecordeddate = readLines(pathdatelastread)
		lastrecordeddate = c(lastrecordeddate[1])
		idxtostart[1] = match(lastrecordeddate[1],days)
		print(paste('Date read is',lastrecordeddate[1],'and idxtostart is',idxtostart[1]))
	}
}
checkdir(pathwrite)
colnames = colnames(read.csv(paste(path,days[1],sep="/"),header = T, skip=6,stringsAsFactors=T))
colnames = colnames[-c(1,2)]
rowtemp = rep(NA,(length(colnames)))
x=1
stnname =  "[IN-018X] "
{
	if(idxtostart[1] <= length(days))
	{
		print(paste('idxtostart[1] is',idxtostart[1],'while length of days1 is',length(days)))
		for(x in idxtostart[1] : length(days))
		{
 		 stitchFile(path,days[x],pathwrite,0)
		 print(days[x])
		}
		lastrecordeddate[1] = as.character(days[x])
		print('Meter 1 DONE')
	}
	else if((!(idxtostart[1] < length(days))))
	{
		print('No historical backlog')
	}
}

print('_________________________________________')
print('Historical Calculations done')
print('_________________________________________')
reprint = 1
print(lastrecordeddate)
write(lastrecordeddate,pathdatelastread)
print('File Written')
while(1)
{
  print('Checking FTP for new data')
  filefetch = dumpftp(days,path)
	if(filefetch == 0)
	{
		Sys.sleep(600)
		next
	}
	print('FTP Check complete')
	daysnew = dir(path)
	daysnew = daysnew[grepl("csv",daysnew)]
	if(length(daysnew) == length(days))
	{
	  if(reprint == 1)
		{
			reprint = 0
			print('Waiting for new file dump')
		}
		Sys.sleep(600)
		next
	}
	reprint = 1
	match = match(days,daysnew)
	days = daysnew
	daysnew = daysnew[-match]
	for(x in 1 : length(daysnew))
	{
	  print(paste('Processing',daysnew[x]))
		stitchFile(path,daysnew[x],pathwrite,1)
		print(paste('Done',daysnew[x]))
	}
	if(length(daysnew) < 1)
		daysnew = lastrecordeddate[1]
	write(c(as.character(daysnew[length(daysnew)])),pathdatelastread)
	lastrecordeddate[1] = as.character(daysnew[length(daysnew)])
gc()
}
print('Out of loop')
sink()
