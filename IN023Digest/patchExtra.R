rm(list = ls(all=T))

reworkPatch = function(data)
{
	tm = as.character(data[,1])
	tm2 = unlist(strsplit(tm,"/"))
	seq1 = seq(from=1,to=length(tm2),by=3)
	tmnew = paste(tm2[(seq1+2)],tm2[(seq1+1)],tm2[seq1],sep="-")
	data[,1] = tmnew
	return(data)
}

pathOrig = "/home/admin/Dropbox/FlexiMC_Data/Fourth_Gen/[IN-023C]/[IN-023C]-lifetime.txt"
#pathOrig = "/tmp/lifetime.txt"
pathPatch = "/home/admin/Dropbox/Misc/IN-023 healthy data input.csv"

dataread = read.table(pathOrig,header=T,sep="\t")
dataPatch = read.table(pathPatch,header=T,sep=",")
rowTemplate = rep(NA,ncol(dataread))
rowTemplate[ncol(rowTemplate)] = "IN-023C"
tmPathRead = as.character(dataread[,1])
tmNow = as.character(format(Sys.time(),tz = "Singapore"))
dataPatch = reworkPatch(dataPatch)
for(x in 1 : nrow(dataPatch))
{
	tmCmpr = as.character(dataPatch[x,1])
	idxmtch = match(tmCmpr,tmPathRead)
	tmNow = as.character(format(Sys.time(),tz = "Singapore"))
	if(is.finite(idxmtch))
	{
		dataread[idxmtch,10] = dataPatch[x,2]
		dataread[idxmtch,15] = dataPatch[x,3]
		dataread[idxmtch,(ncol(dataread)-1)] = tmNow
	}
	else
	{
		rowCnt = nrow(dataread)+1
		rowTemplate[1] = as.character(dataPatch[x,1])
		rowTemplate[10] = as.character(dataPatch[x,2])
		rowTemplate[15] = as.character(dataPatch[x,3])
		rowTemplate[(length(rowTemplate)-1)] = tmNow
		rowTemplate[(length(rowTemplate))] = "IN-023C"
		df = data.frame(rbind(rowTemplate))
		colnames(df) = colnames(dataread)
		#dataread = rbind(dataread,df)
		write.table(df,file=pathOrig,row.names=F,col.names=F,sep="\t",append=T)
	}
}
dataread = read.table(pathOrig,header=T,sep="\t")
dataread = dataread[order(as.character(dataread[,1])),]
write.table(dataread,file=pathOrig,row.names=F,col.names=T,sep="\t",append=F)
