require('RCurl')
require('utils')
require('R.utils')
source('/home/admin/CODE/MasterMail/timestamp.R')
source('/home/admin/CODE/Misc/memManage.R')
FIREMAIL = 0
RESETDAY = 0
NUMBACKLOG=3000
DAYSGLOBAL = " "
retryCnt = 0
serverdownmail = function()
{
	send.mail(from = 'operations@cleantechsolar.com',
	          to = c('operations@cleantechsolar.com'),
						subject = "IN-008X FTP server down",
						body = " ",
						smtp = list(host.name = "smtp.office365.com", port = 587, user.name = 'shravan.karthik@cleantechsolar.com', passwd = 'CTS&*(789', tls = TRUE),
						authenticate = TRUE,
						send = TRUE,																												
						debug = F)
}
serverupmail = function()
{
	send.mail(from = 'operations@cleantechsolar.com',
	          to = c('operations@cleantechsolar.com'),
						subject = "IN-008X FTP server up and running",
						body = " ",
						smtp = list(host.name = "smtp.office365.com", port = 587, user.name = 'shravan.karthik@cleantechsolar.com', passwd = 'CTS&*(789', tls = TRUE),
						authenticate = TRUE,
						send = TRUE,																												
						debug = F)
}
dumpftp = function(path)
{
	print('Calling function')
  url = "ftp://IN-008X:xkAdXUiUvjufydg4@52.148.87.44"
  filenames = try(evalWithTimeout(getURLContent(url,ftp.use.epsv = FALSE,dirlistonly = TRUE),
	 timeout = 600, onTimeout = "error"),silent=T)
	print('Call done')
	if(class(filenames) == 'try-error')
	{
		print('Error in FTP server, will reconnect in 10 mins')
		retryCnt <<- retryCnt + 1
		if(FIREMAIL==0 && retryCnt > 3)
		{
			serverdownmail()
			FIREMAIL<<-1
		}
		return(0)
	}
	retryCnt <<-0
	if(FIREMAIL==1)
	{
		print('Server Back running...')
		serverupmail()
		FIREMAIL<<-0
	}
	if(class(filenames)!='character')
	{
		print('Filenames fetched not of string type')
		return(1)
	}
	print('Passed test')

	recordTimeMaster("IN-008X","FTPProbe")
	newfilenames = unlist(strsplit(filenames,"\n"))
	
	print('Strsplit done')
  newfilenames = newfilenames[grepl("csv",newfilenames)]
	print('Csv match found')
	newfilenames = newfilenames[grepl("ALP",newfilenames)]
	days1a = tail(newfilenames[grepl('Meter-1',newfilenames)],n=NUMBACKLOG)
	days2a = tail(newfilenames[grepl('Meter-2',newfilenames)],n=NUMBACKLOG)
	newfilenames = c(days1a,days2a)
	newfilenames = substr(newfilenames,regexpr('ALP',newfilenames),nchar(newfilenames))

  match = match(newfilenames,DAYSGLOBAL)
	print('Matches found')
	idxmtchs = which(match%in%NA)
  if(length(idxmtchs) < 1)
	{
	  print('No new files')
		return(1)
	}
  newfilenames = newfilenames[idxmtchs]
	for(y in 1 : length(newfilenames))
  {
    urlnew = paste(url,newfilenames[y],sep="/")
    isDownloaded = try(download.file(urlnew,destfile = paste(path,newfilenames[y],sep="/")),silent=T)
		if(class(isDownloaded) == 'try-error')
		{
			print(paste('Error downloading',newfilenames[y]))
			next
		}
  }
	recordTimeMaster("IN-008X","FTPNewFiles",as.character(newfilenames[length(newfilenames)]))
	rm(filenames,newfilenames,day1a,days2a)
	gc()
	return(1)
}
