import requests, json
import requests.auth
import pandas as pd
import numpy as np
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import ast
import logging

tz = pytz.timezone('Asia/Singapore')
path='/home/admin/Dropbox/Second Gen/'
path_write='/home/admin/Dropbox/Lifetime/Gen-1/'
path_raw='/home/admin/Dropbox/SERIS_Live_Data/'
lifetimepath='/home/pranav/ctsfork/Lifetime/Lifetime.csv'



dflifetime=pd.read_csv(lifetimepath)

while(1):
    dflifetime=pd.read_csv(lifetimepath)
    stations=dflifetime['Seris_Station_Name'].dropna().tolist()
    metercols=[]
    metercols2=[]
    raw_cols=[]
    b=dflifetime['Meter_Cols'].dropna().tolist()
    for i in b:
        temp=ast.literal_eval(i)
        metercols.append(temp)
    c=dflifetime['Meter_Cols2'].dropna().tolist()
    for i in c:
        temp=ast.literal_eval(i)
        metercols2.append(temp)
    d=dflifetime['Gen1_Columns'].dropna().tolist()
    for i in d:
        temp=ast.literal_eval(i)
        raw_cols.append(temp)
    nometers=dflifetime['Seris_No_Meters'].dropna().tolist()
    nometers=list(map(int, nometers))
    for index,n in enumerate(stations):
        if(os.path.exists(path_write+n[1:7]+"-LT.txt")):
            pass
        else:
            print(n)
            if(n[-2]=='X'):
                path='/home/admin/Dropbox/Third Gen/'
                for i in sorted(os.listdir(path+n)):
                    for j in sorted(os.listdir(path+n+'/'+i)):
                        df=pd.read_csv(path+n+'/'+i+'/'+j,sep='\t',engine='python')
                        df1 = df[metercols[index]].copy()
                        df1.columns=metercols2[index]
                        df1.insert(2, "GHI-Flag", 0)
                        for m in range(int(nometers[index])):
                            df1.insert(2+(2*(m+1)),metercols2[index][m+2]+'-Flag',0)
                        for m in range(int(nometers[index])):
                            df1.insert(2+2*nometers[index]+(2*(m+1)),metercols2[index][m+2+nometers[index]]+'-Flag',0)
                        df1['Master-Flag']=0
                        if(os.path.exists(path_write+n[1:7]+"-LT.txt")):
                            df1.to_csv(path_write+n[1:7]+"-LT.txt",sep='\t',mode='a',index=False,header=False)
                        else:
                            df1.to_csv(path_write+n[1:7]+"-LT.txt",sep='\t',mode='a',index=False,header=True)
            elif(n[-2]=='S'):
                path='/home/admin/Dropbox/Second Gen/'
                for i in sorted(os.listdir(path+n)):
                    for j in sorted(os.listdir(path+n+'/'+i)):
                        for k in sorted(os.listdir(path+n+'/'+i+'/'+j)):
                            if(len(k)==18):
                                df=pd.read_csv(path+n+'/'+i+'/'+j+'/'+k,sep='\t')
                                print(metercols[index])
                                df1 = df[metercols[index]].copy()
                                df1.columns=metercols2[index]
                                df1.insert(2, "GHI-Flag", 0)
                                for m in range(int(nometers[index])):
                                    print(type(m))
                                for m in range(int(nometers[index])):
                                    df1.insert(2+(2*(m+1)),metercols2[index][m+2]+'-Flag',0)
                                for m in range(int(nometers[index])):
                                    df1.insert(2+2*nometers[index]+(2*(m+1)),metercols2[index][m+2+nometers[index]]+'-Flag',0)
                                df1['Master-Flag']=0
                                if(os.path.exists(path_write+n[1:7]+"-LT.txt")):
                                    df1.to_csv(path_write+n[1:7]+"-LT.txt",sep='\t',mode='a',index=False,header=False)
                                else:
                                    df1.to_csv(path_write+n[1:7]+"-LT.txt",sep='\t',mode='a',index=False,header=True)

    timenow=datetime.datetime.now(tz)+datetime.timedelta(hours=-3.5)
    timenowstr=str(timenow)
    timenowdate=str(timenow.date())
    timenowdate2=timenow.strftime("%d-%m-%Y")

    for index,n in enumerate(stations):
        try:
            print(n,timenowdate)
            if(n[-2]=='S'):
                path='/home/admin/Dropbox/Second Gen/'
            elif(n[-2]=='X'):
                path='/home/admin/Dropbox/Third Gen/'
            for i in range(1,15):
                df2=pd.read_csv(path_write+n[1:7]+"-LT.txt",sep='\t')
                df2_temp=df2.fillna(0)
                pastdate=timenow+datetime.timedelta(days=-i)
                pastdatestr=str(pastdate)
                pastdatestr2=pastdate.strftime("%d-%m-%Y")
                if(n[-2]=='S'):
                    path2=path+n+'/'+pastdatestr[0:4]+'/'+pastdatestr[0:7]+'/'+n+' '+pastdatestr[2:4]+pastdatestr[5:7]+'.txt'
                elif(n[-2]=='X'):
                    path2=path+n+'/'+pastdatestr[0:4]+'/'+n+' '+pastdatestr[0:4]+'-'+pastdatestr[5:7]+'.txt'
                if(os.path.exists(path2)):
                    df=pd.read_csv(path2,sep='\t')
                else:
                    continue
                df1 = df[metercols[index]].copy()
                df1.columns=metercols2[index]
                #Flags
                df1.insert(2, "GHI-Flag", 0)
                for m in range(int(nometers[index])):
                    df1.insert(2+(2*(m+1)),metercols2[index][m+2]+'-Flag',0)
                for m in range(int(nometers[index])):
                    df1.insert(2+2*nometers[index]+(2*(m+1)),metercols2[index][m+2+nometers[index]]+'-Flag',0)
                df1['Master-Flag']=0
                if(((df2_temp['Date']==pastdatestr[0:10]) & (df2_temp['Master-Flag'].astype(int)>0)).any() or ((df2_temp['Date']==pastdatestr2[0:10]) & (df2_temp['Master-Flag'].astype(int)>0)).any()):
                    pass
                elif(((df2['Date']==pastdatestr[0:10]).any() or df2['Date']==pastdatestr2[0:10]).any()):
                    try:
                        df3=df1.loc[(df1['Date']==pastdatestr[0:10]) | (df1['Date']==pastdatestr2[0:10])]
                        vals=df3.values.tolist()
                        df2.loc[(df2['Date']==pastdatestr[0:10])|(df2['Date']==pastdatestr2[0:10]),df2.columns.tolist()]=vals[0]
                        df2.to_csv(path_write+n[1:7]+"-LT.txt",sep='\t',mode='w',index=False,header=True)
                    except:
                        logging.exception("Error") 
                        
                else:
                    df3=df1.loc[(df1['Date']==pastdatestr[0:10]) | (df1['Date']==pastdatestr2[0:10])]
                    if(df3.empty):
                        pass
                    else:
                        df3.to_csv(path_write+n[1:7]+"-LT.txt",sep='\t',mode='a',index=False,header=False)
            if(n[-2]=='X'):
                path2=path+n+'/'+timenowstr[0:4]+'/'+n+' '+timenowstr[0:4]+'-'+timenowstr[5:7]+'.txt'
                if(os.path.exists(path2)):
                    df=pd.read_csv(path2,sep='\t')
                    df1 = df[metercols[index]].copy()
                    df1.columns=metercols2[index]
                    #Flags
                    df1.insert(2, "GHI-Flag", 0)
                    for m in range(int(nometers[index])):
                        df1.insert(2+(2*(m+1)),metercols2[index][m+2]+'-Flag',0)
                    for m in range(int(nometers[index])):
                        df1.insert(2+2*nometers[index]+(2*(m+1)),metercols2[index][m+2+nometers[index]]+'-Flag',0)
                    df1['Master-Flag']=0
                    if((df2['Date']==timenowdate).any() or (df2['Date']==timenowdate2).any()):
                        try:
                            df3=df1.loc[(df1['Date']==timenowdate[0:10]) | (df1['Date']==timenowdate[0:10])]
                            vals=df3.values.tolist()
                            df2.loc[(df2['Date']==timenowdate[0:10])|(df2['Date']==timenowdate[0:10]),df2.columns.tolist()]=vals[0]
                            df2.to_csv(path_write+n[1:7]+"-LT.txt",sep='\t',mode='w',index=False,header=True)
                        except:
                            logging.exception("Error") 
                    else:
                        df3=df1.loc[df1['Date']==timenowdate]
                        if(df3.empty):
                            pass
                        else:
                            df3.to_csv(path_write+n[1:7]+"-LT.txt",sep='\t',mode='a',index=False,header=False)
            elif(n[-2]=='S'):
                try:
                    df_lt=pd.read_csv(path_write+n[1:-2]+'-LT.txt',sep='\t')
                    raw_columns=raw_cols[index]
                    lt_columns=metercols2[index]
                    time_now=datetime.datetime.now(tz)
                    date=str(time_now.date())
                    df=pd.read_csv(path_raw+n+'/'+date[0:4]+'/'+date[0:7]+'/'+n+' '+date+'.txt',sep='\t')
                    df=df[raw_columns]
                    df=df.rename(columns = {raw_columns[0]:'Date'})
                    dict_temp={'Date':[date],'GHI':[round(float(df[raw_columns[1]].sum())/60000,2)],'GHI-Flag':[0],'LastT':[df['Date'].tail(1).values[0]],'DA':[round(float(len(df))/14.40,1)]}
                    lt_columns.insert(2,'GHI-Flag')
                    df['Date'] = df['Date'].str[:-5]
                    if(n=='[SG-005S]'):
                        for index2 in range(5):
                            if(len(df[raw_columns[2+index2]].dropna().tail(1).values)>0):
                                lastr=df[raw_columns[2+index2]].dropna().tail(1).values[0]
                                eac=df[raw_columns[2+index2]].dropna().tail(1).values[0]-df[raw_columns[2+index2]].dropna().head(1).values[0]
                            else:
                                lastr=np.nan
                                eac=np.nan
                            dict_temp[lt_columns[3+index2]]=lastr
                            dict_temp[lt_columns[3+nometers[index]+index2]]=eac
                    else:
                        for index2 in range(nometers[index]):
                            if(len(df[raw_columns[2+index2]].dropna().tail(1).values)>0):
                                lastr=df[raw_columns[2+index2]].dropna().tail(1).values[0]
                                eac=df[raw_columns[2+index2]].dropna().tail(1).values[0]-df[raw_columns[2+index2]].dropna().head(1).values[0]
                            else:
                                lastr=np.nan
                                eac=np.nan
                            dict_temp[lt_columns[3+index2]]=lastr
                            dict_temp[lt_columns[3+nometers[index]+index2]]=eac
                    df_template = pd.DataFrame(dict_temp,columns =lt_columns)
                    for m in range(nometers[index]):
                        df_template.insert(2+(2*(m+1)),lt_columns[3+m]+'-Flag',0)
                    for m in range(nometers[index]):
                        df_template.insert(2+2*nometers[index]+(2*(m+1)),lt_columns[3+nometers[index]+m]+'-Flag',0)
                    df_template['Master-Flag']=0
                    if((df_lt['Date']==date).any()):
                        df_lt.loc[df_lt['Date']==date,:]=df_template.values[0]
                        df_lt.to_csv(path_write+n[1:-2]+"-LT.txt",sep='\t',mode='w',index=False,header=True)
                    else:
                        df_template.to_csv(path_write+n[1:-2]+"-LT.txt",sep='\t',mode='a',index=False,header=False)
                except:
                    logging.exception("Error")  
        except:
            logging.exception("Error")
    time.sleep(60)

