import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys

tz = pytz.timezone('Asia/Calcutta')
path='/home/admin/Dropbox/Fourth_Gen/[IN-052L]/[IN-052L]-lifetime.txt'
path2='/home/admin/Dropbox/Lifetime/'
a=[]

if(os.path.exists(path2+"IN-052-LT.txt")):
    pass
else:
    df=pd.read_csv(path,sep='\t')
    df1 = df[['Date','WMS_1.GTI','MFM_1.LastRead','MFM_2.LastRead','MFM_1.Eac1','MFM_2.Eac1','MFM_1.LastTime','MFM_1.DA']].copy()
    df1.columns=['Date','GHI','LastR-MFM1','LastR-MFM2','Eac-MFM1','Eac1-MFM2','LastT','DA']
    print(df1)
    df1.insert(2, "GHI-Flag", 0)
    df1.insert(4, "LastR-MFM1-Flag", 0)
    df1.insert(6, "LastR-MFM2-Flag", 0)   
    df1['Master-Flag']=0
    df1.to_csv(path2+"IN-052-LT.txt",sep='\t',mode='a',index=False,header=True)

#Historical
df2=pd.read_csv(path2+'IN-052-LT.txt',sep='\t')
startdate=(df2['Date'][len(df2['Date'])-1])
start=startdate
start=datetime.datetime.strptime(start, "%Y-%m-%d").date()
print(start)
end=datetime.datetime.now(tz).date()
print(end)
tot=end-start
tot=tot.days
print('Diff day is,',tot)
for n in range(tot):
    strstart=str(start)
    df=pd.read_csv(path,sep='\t')
    df1 = df[['Date','WMS_1.GTI','MFM_1.LastRead','MFM_2.LastRead','MFM_1.Eac1','MFM_2.Eac1','MFM_1.LastTime','MFM_1.DA']].copy()
    df1.columns=['Date','GHI','LastR-MFM1','LastR-MFM2','Eac-MFM1','Eac1-MFM2','LastT','DA']
    print(df1)
    df1.insert(2, "GHI-Flag", 0)
    df1.insert(4, "LastR-MFM1-Flag", 0)
    df1.insert(6, "LastR-MFM2-Flag", 0)   
    df1['Master-Flag']=0
    if((df2['Date']==strstart).any()):
        print("Skipped")
        pass
    else:
        df3=df1.loc[df1['Date']==strstart]
        df3.to_csv(path2+'IN-052-LT.txt',sep='\t',mode='a',index=False,header=False)
        print(df3)
    start=start+datetime.timedelta(days=1)


while(1):
    timenow=datetime.datetime.now(tz)+datetime.timedelta(hours=-3.5)
    timenowstr=str(timenow)
    timenowdate=str(timenow.date())
    print(timenowdate)
    df=pd.read_csv(path,sep='\t')
    df2=pd.read_csv(path2+'IN-052-LT.txt',sep='\t')
    df1 = df[['Date','WMS_1.GTI','MFM_1.LastRead','MFM_2.LastRead','MFM_1.Eac1','MFM_2.Eac1','MFM_1.LastTime','MFM_1.DA']].copy()
    df1.columns=['Date','GHI','LastR-MFM1','LastR-MFM2','Eac-MFM1','Eac1-MFM2','LastT','DA']
    print(df1)
    df1.insert(2, "GHI-Flag", 0)
    df1.insert(4, "LastR-MFM1-Flag", 0)
    df1.insert(6, "LastR-MFM2-Flag", 0)   
    df1['Master-Flag']=0
    print(df2['Date'])
    if((df2['Date']==timenowdate).any()):
        print('PRESENT!')
        df3=df1.loc[df1['Date']==timenowdate]
        print(df3)
        vals=df3.values.tolist()
        df2.loc[df2['Date']==timenowdate,df2.columns.tolist()]=vals[0]
        df2.to_csv(path2+'IN-052-LT.txt',sep='\t',mode='w',index=False,header=True)
    else:
        print('ABSENT')
        df3=df1.loc[df1['Date']==timenowdate]
        if(df3.empty):
            print("Waiting")
        else:
            print("Write")
            df3.to_csv(path2+'IN-052-LT.txt',sep='\t',mode='a',index=False,header=False)
            print(df3)
    time.sleep(1800)
