import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import ast
import logging

tz = pytz.timezone('Asia/Calcutta')
path='/home/admin/Dropbox/FlexiMC_Data/Fourth_Gen/'
path2='/home/admin/Dropbox/Lifetime/Gen-1/'
lifetimepath='/home/pranav/Lifetime3.csv'
path_IN050='/home/admin/Dropbox/FlexiMC_Data/Gen1_Data/[IN-050C]/'
path_write_IN050='/home/admin/Dropbox/Lifetime/Gen-1/IN-050-LT.txt'

def get_IN050_data(date):
    meters=[]
    ordered_cols=[]
    df_final=pd.DataFrame()
    for k in sorted(os.listdir(path_IN050+date[0:4]+'/'+date[0:7])):
        if (('Inverter' not in k) and ('WMS' not in k)):
            meters.append(k)
            if(os.path.exists(path_IN050+date[0:4]+'/'+date[0:7]+'/'+k+'/[IN-050C]-'+k+'-'+date+'.txt')):
                df=pd.read_csv(path_IN050+date[0:4]+'/'+date[0:7]+'/'+k+'/[IN-050C]-'+k+'-'+date+'.txt',sep='\t')
                eac=df['Energy_Export'].dropna().iloc[-1]-df['Energy_Export'].dropna().iloc[0]
                if(k=='SL_MFM1' or k=='AMB_MFM1'):
                    eac=round(eac,1)
                    lastr=round(df['Energy_Export'].dropna().iloc[-1],3)
                else:
                    eac=round(eac/1000,1)
                    lastr=round(df['Energy_Export'].dropna().iloc[-1]/1000,3)
                date=str(df['Tstamp'].dropna().iloc[0])[0:10]
                df_eac=pd.DataFrame({'Date':[date],k+'-Eac':[eac],k+'-LastR':[lastr],'Name':[k]},columns =['Date',k+'-Eac',k+'-LastR','Name'])
            else:
                df_eac=pd.DataFrame({'Date':[date],k+'-Eac':[0],k+'-LastR':[0],'Name':[k]},columns =['Date',k+'-Eac',k+'-LastR','Name'])
            df_final=df_final.append(df_eac,sort=True)
    df_final=df_final.groupby(['Date'], as_index=False).sum()
    df_final['GHI']=0
    if(os.path.exists(path_IN050+date[0:4]+'/'+date[0:7]+'/Bot_WMS/[IN-050C]-Bot_WMS-'+date+'.txt')):
        wms_df=pd.read_csv(path_IN050+date[0:4]+'/'+date[0:7]+'/Bot_WMS/[IN-050C]-Bot_WMS-'+date+'.txt',sep='\t')
        ghi=wms_df['Irradiation_Tilt1_Actual'].sum()/12000
        df_final['GHI']=round(ghi,2)
    for i in range(2):
        for j in meters:
            if(i==0):
                ordered_cols.append(j+'-LastR')
            else:
                ordered_cols.append(j+'-Eac')
    ordered_cols.insert(0,'GHI')
    ordered_cols.insert(0,'Date')
    df_final=df_final[ordered_cols]
    for i in range(2):
        for index,j in enumerate(meters):
            if(i==0):
                df_final.insert(3+2*index,j+'-LastR-Flag',0)
            else:
                df_final.insert(3+2*len(meters)+2*index,j+'-Eac-Flag',0)
    df_final.insert(2, "GHI-Flag", 0)
    df_final['Master-Flag']=0
    return df_final

def update_IN050_date(date,timenow):
    df_write=pd.read_csv(path_write_IN050,sep='\t')
    for i in range(15,0,-1):
        pastdate=timenow+datetime.timedelta(days=-i)
        pastdatestr=str(pastdate)
        pastdatestr2=pastdate.strftime("%d-%m-%Y")
        if(((df_write['Date']==pastdatestr[0:10]).any() or df_write['Date']==pastdatestr2[0:10]).any()):
            pass
        else:
            df3=get_IN050_data(pastdatestr[0:10])
            if(df3.empty):
                print('Empty')
                pass
            else:
                print('Adding')
                df3.to_csv('/home/admin/Dropbox/Lifetime/Gen-1/IN-050-LT.txt',sep='\t',mode='a',index=False,header=False)

    if((df_write['Date']==date).any()):
        print('PRESENT!')
        df3=get_IN050_data(date)
        vals=df3.values.tolist()
        df_write.loc[df_write['Date']==date,df_write.columns.tolist()]=vals[0]
        df_write.to_csv(path_write_IN050,sep='\t',mode='w',index=False,header=True)
    else:
        print('ABSENT')
        df3=get_IN050_data(date)
        if(df3.empty):
            print("Waiting")
        else:
            df3.to_csv(path_write_IN050,sep='\t',mode='a',index=False,header=False)

while(1):
    dflifetime=pd.read_csv(lifetimepath)
    print(dflifetime.columns)
    a=dflifetime['Flexi_Station_Name'].dropna().tolist()
    irr=dflifetime['Flexi_Irradiation'].tolist()
    metercols=[]
    b=dflifetime['Meter_Cols_Flexi'].dropna().tolist()
    for i in b:
        temp=ast.literal_eval(i)
        metercols.append(temp)
    print(a)
    print(irr)
    print(metercols)
    for index,n in enumerate(a):
        try:
            if(os.path.exists(path2+n[1:7]+"-LT.txt")):
                pass
            else:
                print(n)
                df=pd.read_csv(path+n+'/'+n+'-lifetime.txt',sep='\t',engine='python')
                if 'WMS.GTI' in df.columns or 'WMS-GTI' in df.columns and n!='[IN-020C]':
                    if 'WMS-GTI' in df.columns:
                        cols=['Date','WMS-GTI',metercols[index][0]+'.LastTime',metercols[index][0]+'.DA']
                    else:
                        cols=['Date','WMS.GTI',metercols[index][0]+'.LastTime',metercols[index][0]+'.DA']
                    cols2=['Date','GHI','LastT','DA']
                    for m in range(len(metercols[index])):
                        str1=metercols[index][m]+'.LastRead'
                        str2='LastR-'+metercols[index][m]
                        cols.insert(2+m,str1)
                        cols2.insert(2+m,str2)
                    for m in range(len(metercols[index])):
                        str1=metercols[index][m]+'.Eac1'
                        str2='Eac-'+metercols[index][m]
                        cols.insert(2+len(metercols[index])+m,str1)
                        cols2.insert(2+len(metercols[index])+m,str2)
                    df1 = df[cols].copy()
                    df1.columns=cols2
                    df1.insert(2, "GHI-Flag", 0)
                    for m in range(len(metercols[index])):
                        df1.insert(2+(2*(m+1)), "LastR-"+metercols[index][m]+"-Flag", 0)
                    for m in range(len(metercols[index])):
                        df1.insert(2+2*len(metercols[index])+(2*(m+1)),"Eac-"+metercols[index][m]+'-Flag',0)
                    df1['Master-Flag']=0
                    df1.to_csv(path2+n[1:7]+"-LT.txt",sep='\t',mode='a',index=False,header=True)
                else:
                    print(n,'Loop2')
                    dfdate=df[['Date']]
                    if(n=='[IN-016C]' or n=='[IN-070C]'):
                        df2=pd.read_csv(path2+irr[index]+'-LT.txt',sep='\t',engine='python')
                        finaldf=pd.merge(dfdate, df2, on=['Date'],sort=False,how='left')
                        finaldf=finaldf[['Date','GHI']]
                        print(finaldf)
                    elif(n=='[IN-004C]' or n=='[IN-005C]' or n=='[IN-006C]' or n=='[IN-007C]'):
                        df_irr=pd.DataFrame(columns=['Date','Gsi01'])
                        if(n=='[IN-004C]'):
                            path_2='/home/admin/Dropbox/Second Gen/[IN-711S]/'
                        elif(n=='IN-006C'):
                            path_2='/home/admin/Dropbox/Second Gen/[IN-713S]/'
                        else:
                            path_2='/home/admin/Dropbox/Second Gen/[IN-721S]/'
                        for i in sorted(os.listdir(path_2)):
                            for j in sorted(os.listdir(path_2+i)):
                                for k in sorted(os.listdir(path_2+i+'/'+j)):
                                    if(len(k)<18):
                                        try:
                                            print(path_2+i+'/'+j+'/'+k)
                                            df_temp_irr=pd.read_csv(path_2+i+'/'+j+'/'+k,sep='\t')
                                            df_irr=df_irr.append(df_temp_irr[['Date','Gsi01']])
                                        except:
                                            pass
                                    else:
                                        pass
                        finaldf=pd.merge(dfdate, df_irr, on=['Date'],sort=False,how='left')
                        finaldf=finaldf[['Date','Gsi01']]
                    else:
                        df2=pd.read_csv(path+irr[index]+'/'+irr[index]+'-lifetime.txt',sep='\t',engine='python')
                        finaldf=pd.merge(dfdate, df2, on=['Date'],sort=False,how='left')
                        finaldf=finaldf[['Date','WMS.GTI']]
                    if(n=='[IN-007C]'):
                        cols=['Date',metercols[index][0]+'-LastTime',metercols[index][0]+'-DA']
                    else:
                        cols=['Date',metercols[index][0]+'.LastTime',metercols[index][0]+'.DA']
                    cols2=['Date','LastT','DA']
                    for m in range(len(metercols[index])):
                        if(n=='[IN-007C]'):
                            str1=metercols[index][m]+'-LastRead'
                        else:
                            str1=metercols[index][m]+'.LastRead'
                        str2='LastR-'+metercols[index][m]
                        cols.insert(1+m,str1)
                        cols2.insert(1+m,str2)
                    for m in range(len(metercols[index])):
                        if(n=='[IN-007C]'):
                            str1=metercols[index][m]+'-Eac1'
                        else:
                            str1=metercols[index][m]+'.Eac1'
                        str2='Eac-'+metercols[index][m]
                        cols.insert(1+len(metercols[index])+m,str1)
                        cols2.insert(1+len(metercols[index])+m,str2)
                    df1 = df[cols].copy()
                    df1.columns=cols2
                    dfmerge=pd.merge(finaldf, df1, on=['Date'],sort=False,how='left')
                    dfmerge.insert(2, "GHI-Flag", 0)
                    for m in range(len(metercols[index])):
                        dfmerge.insert(2+(2*(m+1)), "LastR-"+metercols[index][m]+"-Flag", 0)
                    for m in range(len(metercols[index])):
                        dfmerge.insert(2+2*len(metercols[index])+(2*(m+1)),"Eac-"+metercols[index][m]+'-Flag',0)
                    dfmerge['Master-Flag']=0
                    dfmerge.to_csv(path2+n[1:7]+"-LT.txt",sep='\t',mode='a',index=False,header=True)
        except Exception as e:
            print(e)
            logging.exception('Historical')
    timenow=datetime.datetime.now(tz)+datetime.timedelta(hours=-3.5)
    timenowstr=str(timenow)
    timenowdate=str(timenow.date())
    print(timenowdate)
    update_IN050_date(timenowdate,timenow) #Updating IN-050 LT file
    #print('IN-050 Failed')
    for index,n in enumerate(a):
        try:
            print(n)
            if(n=='Down'):
                continue
            df=pd.read_csv(path+n+'/'+n+'-lifetime.txt',sep='\t',engine='python')
            try:
                df2=pd.read_csv(path2+n[1:7]+"-LT.txt",sep='\t',engine='python')
            except:
                pass
            if 'WMS.GTI' in df.columns or 'WMS-GTI' in df.columns and n!='[IN-020C]' : 
                if 'WMS-GTI' in df.columns:
                    cols=['Date','WMS-GTI',metercols[index][0]+'.LastTime',metercols[index][0]+'.DA']
                else:
                    cols=['Date','WMS.GTI',metercols[index][0]+'.LastTime',metercols[index][0]+'.DA']
                cols2=['Date','GHI','LastT','DA']
                for m in range(len(metercols[index])):
                    str1=metercols[index][m]+'.LastRead'
                    str2='LastR-'+metercols[index][m]
                    cols.insert(2+m,str1)
                    cols2.insert(2+m,str2)
                for m in range(len(metercols[index])):
                    str1=metercols[index][m]+'.Eac1'
                    str2='Eac-'+metercols[index][m]
                    cols.insert(2+len(metercols[index])+m,str1)
                    cols2.insert(2+len(metercols[index])+m,str2)
                df1 = df[cols].copy()
                df1.columns=cols2
                df1.insert(2, "GHI-Flag", 0)
                for m in range(len(metercols[index])):
                    df1.insert(2+(2*(m+1)), "LastR-"+metercols[index][m]+"-Flag", 0)
                for m in range(len(metercols[index])):
                    df1.insert(2+2*len(metercols[index])+(2*(m+1)),"Eac-"+metercols[index][m]+'-Flag',0)
                df1['Master-Flag']=0
                for i in range(15,0,-1):
                    pastdate=timenow+datetime.timedelta(days=-i)
                    pastdatestr=str(pastdate)
                    pastdatestr2=pastdate.strftime("%d-%m-%Y")
                    if(((df2['Date']==pastdatestr[0:10]).any() or df2['Date']==pastdatestr2[0:10]).any()):
                        pass
                    else:
                        df3=df1.loc[(df1['Date']==pastdatestr[0:10]) | (df1['Date']==pastdatestr2[0:10])]
                        if(df3.empty):
                            pass
                        else:
                            df3.to_csv(path2+n[1:7]+"-LT.txt",sep='\t',mode='a',index=False,header=False)
                if((df2['Date']==timenowdate).any()):
                    print('PRESENT!')
                    df3=df1.loc[df1['Date']==timenowdate]
                    vals=df3.values.tolist()
                    print(vals[0])
                    df2.loc[df2['Date']==timenowdate,df2.columns.tolist()]=vals[0]
                    df2.to_csv(path2+n[1:7]+"-LT.txt",sep='\t',mode='w',index=False,header=True)
                else:
                    print('ABSENT')
                    df3=df1.loc[df1['Date']==timenowdate]
                    if(df3.empty):
                        print("Waiting")
                    else:
                        df3.to_csv(path2+n[1:7]+"-LT.txt",sep='\t',mode='a',index=False,header=False)
            else:
                dfdate=df[['Date']]
                if(n=='[IN-016C]' or n=='[IN-070C]'):
                    dfirr=pd.read_csv(path2+irr[index]+'-LT.txt',sep='\t',engine='python')
                    finaldf=pd.merge(dfdate, dfirr, on=['Date'],sort=False,how='left')
                    finaldf=finaldf[['Date','GHI']]
                elif(n=='[IN-004C]' or n=='[IN-005C]' or n=='[IN-006C]' or n=='[IN-007C]'):
                    df_irr=pd.DataFrame(columns=['Date','Gsi01'])
                    if(n=='[IN-004C]'):
                        path_2='/home/admin/Dropbox/Second Gen/[IN-711S]/'
                    elif(n=='IN-006C'):
                        path_2='/home/admin/Dropbox/Second Gen/[IN-713S]/'
                    else:
                        path_2='/home/admin/Dropbox/Second Gen/[IN-721S]/'
                    df_irr=pd.DataFrame(columns=['Date','Gsi01'])
                    for i in sorted(os.listdir(path_2)):
                        for j in sorted(os.listdir(path_2+i)):
                            for k in sorted(os.listdir(path_2+i+'/'+j)):
                                if(len(k)<18):
                                    try:
                                        print(path_2+i+'/'+j+'/'+k)
                                        df_temp_irr=pd.read_csv(path_2+i+'/'+j+'/'+k,sep='\t')
                                        df_irr=df_irr.append(df_temp_irr[['Date','Gsi01']])
                                    except:
                                        pass
                                else:
                                    pass
                    finaldf=pd.merge(dfdate, df_irr, on=['Date'],sort=False,how='left')
                    finaldf=finaldf[['Date','Gsi01']]
                else:
                    dfirr=pd.read_csv(path+irr[index]+'/'+irr[index]+'-lifetime.txt',sep='\t',engine='python')
                    finaldf=pd.merge(dfdate, dfirr, on=['Date'],sort=False,how='left')
                    finaldf=finaldf[['Date','WMS.GTI']]
                if(n=='[IN-007C]'):
                    cols=['Date',metercols[index][0]+'-LastTime',metercols[index][0]+'-DA']
                else:
                    cols=['Date',metercols[index][0]+'.LastTime',metercols[index][0]+'.DA']
                cols2=['Date','LastT','DA']
                for m in range(len(metercols[index])):
                    if(n=='[IN-007C]'):
                        str1=metercols[index][m]+'-LastRead'
                    else:
                        str1=metercols[index][m]+'.LastRead'
                    str2='LastR-'+metercols[index][m]
                    cols.insert(1+m,str1)
                    cols2.insert(1+m,str2)
                for m in range(len(metercols[index])):
                    if(n=='[IN-007C]'):
                        str1=metercols[index][m]+'-Eac1'
                    else:
                        str1=metercols[index][m]+'.Eac1'
                    str2='Eac-'+metercols[index][m]
                    cols.insert(1+len(metercols[index])+m,str1)
                    cols2.insert(1+len(metercols[index])+m,str2)
                df1 = df[cols].copy()
                df1.columns=cols2
                dfmerge=pd.merge(finaldf, df1, on=['Date'],sort=False,how='left')
                dfmerge.insert(2, "GHI-Flag", 0)
                for m in range(len(metercols[index])):
                    dfmerge.insert(2+(2*(m+1)), "LastR-"+metercols[index][m]+"-Flag", 0)
                for m in range(len(metercols[index])):
                    dfmerge.insert(2+2*len(metercols[index])+(2*(m+1)),"Eac-"+metercols[index][m]+'-Flag',0)
                dfmerge['Master-Flag']=0
                for i in range(15,0,-1):
                    pastdate=timenow+datetime.timedelta(days=-i)
                    pastdatestr=str(pastdate)
                    pastdatestr2=pastdate.strftime("%d-%m-%Y")
                    if(((df2['Date']==pastdatestr[0:10]).any() or df2['Date']==pastdatestr2[0:10]).any()):
                        pass
                    else:
                        df3=dfmerge.loc[(dfmerge['Date']==pastdatestr[0:10]) | (dfmerge['Date']==pastdatestr2[0:10])]
                        if(df3.empty):
                            pass
                        else:
                            df3.to_csv(path2+n[1:7]+"-LT.txt",sep='\t',mode='a',index=False,header=False)
                if((df2['Date']==timenowdate).any()):
                    print('PRESENT!')
                    df3=dfmerge.loc[dfmerge['Date']==timenowdate]
                    if(df3.empty):
                        print("Waiting")
                    else:
                        df3=dfmerge.loc[dfmerge['Date']==timenowdate]
                        vals=df3.values.tolist()
                        print(vals[0])
                        print(df2.columns.tolist())
                        df2.loc[df2['Date']==timenowdate,df2.columns.tolist()]=vals[0]
                        df2.to_csv(path2+n[1:7]+"-LT.txt",sep='\t',mode='w',index=False,header=True)
                else:
                    print('ABSENT')
                    df3=dfmerge.loc[dfmerge['Date']==timenowdate]
                    if(df3.empty):
                        print("Waiting")
                    else:
                        df3.to_csv(path2+n[1:7]+"-LT.txt",sep='\t',mode='a',index=False,header=False)
        except Exception as e:
            logging.exception('message')
            print(e)
    time.sleep(1800)
  
   
