getRecipients=function(station,typ)
{
  dataread<-read.table("/home/admin/CODE/Send_mail/mail_recipients.csv",sep=",",header = T,stringsAsFactors = FALSE)
  #dataread<-read.table("/home/saradindu/MEGA/Cleantech_Solar/codebase/Send_mail/mail_recipients.csv",sep=",",header = T,stringsAsFactors = FALSE)
  an=station
  n=colnames(dataread)
  col= ncol(dataread)
  e=c()
  k=col-2
  focur=substr(an,1,2)
  if(focur=="IN")
    k=col-7
  if(focur=="PH")
    k=col-6
  if(focur=="KH")
    k=col-5
  if(focur=="TH")
    k=col-4
  if(focur=="SG")
    k=col-3
  if(typ=="M" || typ=="m")
  {
    for (i in 2:(length(n)-2))
    {
      s=n[i]
      if((substr(s,1,2)==substr(an,1,2))&&(substr(s,4,7)==substr(an,4,7)))
      {
        k=i
        break()
      }
    }
    for (i in 1:(nrow(dataread)))
    {
      if(as.numeric(dataread[i,k])==1)
      {
        if(as.character(dataread[i,nrow(dataread)-1])=="TR" && as.character(dataread[i,nrow(dataread)])=="TR")
        {
          e=c(e,as.character(dataread[i,1]))
        }
        else
        {
          if((Sys.Date()>as.Date(dataread[i,nrow(dataread)-1],,origin="1970-01-01")) && (Sys.Date()>as.Date(dataread[i,nrow(dataread)],,origin="1970-01-01")))
          {
            e=c(e,as.character(dataread[i,1]))
          }
        }

      }
    }
  }
  if(typ=="A" || typ=="a")
  {
    for (i in 2:(length(n)-2))
    {
      s=n[i]
      if((substr(s,1,2)==substr(an,1,2))&&(substr(s,4,7)==substr(an,4,7)))
      {
        k=i+1
        break()
      }
    }
    for (i in 1:(nrow(dataread)))
    {
      if(as.numeric(dataread[i,k])==1)
      {
        if(as.character(dataread[i,nrow(dataread)-1])=="TR" && as.character(dataread[i,nrow(dataread)])=="TR")
        {
          e=c(e,as.character(dataread[i,1]))
        }
        else
        {
          if((Sys.Date()>as.Date(dataread[i,nrow(dataread)-1],origin="1970-01-01")) && (Sys.Date()>as.Date(dataread[i,nrow(dataread)],origin="1970-01-01")))
          {
            e=c(e,as.character(dataread[i,1]))
          }
        }

      }
    }
  }
  return(e)
}