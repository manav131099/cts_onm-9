source('/home/admin/CODE/common/aggregate.R')

registerMeterList("KH-007X",c(""))
aggNameTemplate = getNameTemplate()
aggColTemplate = getColumnTemplate()

aggColTemplate[1] = 1 #Column no for date
aggColTemplate[2] = 2 #Column no for DA
aggColTemplate[3] = 12 #column for LastRead
aggColTemplate[4] = 11 #column for LastTime
aggColTemplate[5] = 3 #column for Eac-1
aggColTemplate[6] = 4 #column for Eac-2
aggColTemplate[7] = 6 #column for Yld-1
aggColTemplate[8] = 7 #column for Yld-2
aggColTemplate[9] = 8 #column for PR-1
aggColTemplate[10] = 9 #column for PR-2
aggColTemplate[11] = 10 #column for Irr
aggColTemplate[12] = "KH-001S" # IrrSrc Value
aggColTemplate[13] = NA #column for Tamb
aggColTemplate[14] = NA #column for Tmod
aggColTemplate[15] = NA #column for Hamb

registerColumnList("KH-007X","",aggNameTemplate,aggColTemplate)
