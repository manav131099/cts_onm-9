rm(list = ls(all=T))
source('/home/admin/CODE/Misc/missingPIDS.R')
require('mailR')
source("/home/admin/CODE/Send_mail/sendmail.R")
Sys.sleep(120)
print('Proceeding')
errHandle = file('/home/admin/Logs/LogsFault.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)

TotProcs = noOfRProcess()
botname = BotNameAll()
ActiveProcs = noOfActiveProcess()
setFlag = c()
listOFAllPID = pidAll()

allreset = function()
{
print(paste('Totprocs',TotProcs))
for(x in 1 : TotProcs)
{
	setFlag[x]<<-0
}
}

sendMailError = function(x,data)
{
if(grepl("_DB",x))
	return(1)
sender = c('operations@cleantechsolar.com')
uname <- 'shravan.karthik@cleantechsolar.com'
recipients = getRecipients("FaultAlert","m")
pwd = 'CTS&*(789'
subjectMail = paste("BOT",x,"RESET")
bodyMail = data
if(data == "DOWN")
{
	subjectMail = paste("BOT",x,"DOWN")
	bodyMail = " "
}
	while(1)
	{
	print('Sending mail')
	mailer = try(send.mail(from = sender,
	          to = recipients,
						subject = subjectMail,
						body = bodyMail,
            smtp = list(host.name = "smtp.office365.com", port = 587, user.name = uname, passwd = pwd, tls = TRUE),
						authenticate = TRUE,
						send = TRUE,																												
						debug = F),silent=T)
		if(class(mailer)=='try-error')
		{
			print('Got mail error')
			Sys.sleep(180)
			next
		}
		break
	}
}

monitorBotsStuck = function()
{
	pathReadMaster = "/home/admin/Start/MasterMail"
	for(x in 1 : length(botname))
	{
		if(setFlag[x] == 1)
			next
		splitString = unlist(strsplit(botname[x],"_"))
		if(length(splitString)<2)
			next
		if(splitString[2]=='History')
		{
			logFileName = paste(splitString[1],"_FTPProbe.txt",sep="")
			filePath = paste(pathReadMaster,logFileName,sep="/")
			if(file.exists(filePath))
			{
				dataread = readLines(filePath)
				timeLast = as.POSIXct(dataread,format="%Y-%m-%d %H:%M:%S",tz="Singapore")
				timeNow = format(Sys.time(),tz="Singapore")
				timeNow = as.POSIXct(timeNow,format="%Y-%m-%d %H:%M:%S",tz="Singapore")
				differenceHours = as.numeric(difftime(timeNow , timeLast,units="hours"))
				if(differenceHours > 5)
				{
					systemCommand = paste("kill -9 ",listOFAllPID[x])
					print(paste('Executing system command :',systemCommand))
					system(systemCommand)
					bodyText = paste("Last time FTP probed successfully",dataread,"(",round(differenceHours,1),"hrs )")
					sendMailError(botname[x],bodyText)
					systemCommand = paste("/home/admin/runscripts.sh",botname[x],"&")
					print(paste('Executing system command :',systemCommand))
					system(systemCommand)
					print('Bot reset')
				}
			}
		}
	}
	TotProcs <<- noOfRProcess()
	botname <<- BotNameAll()
	ActiveProcs <<- noOfActiveProcess()
	listOFAllPID <<- pidAll()
	return()
}

allreset()
toprint = 1
while(1)
{
print('Still alive')
#if(TotProcs == ActiveProcs)
#	{
#		if(toprint)
#			print('All processes active')
#		toprint=0
#		Sys.sleep(3600)
#		next
#	}
	PIDsMiss = missingFunction()
	if(length(PIDsMiss))
	{
		pidAlone = c()
		print('Processes are down')
		for(x in 1 : length(PIDsMiss))
		{
			pidAlone[x] = unlist(strsplit(PIDsMiss[x]," "))[1]
		}
		idxmatch = match(as.numeric(pidAlone),as.numeric(listOFAllPID))
		print(paste('Idxmatch is',idxmatch))
		idxmatch = idxmatch[complete.cases(idxmatch)]
		if(!length(idxmatch))
			next
		for(x in 1 : length(idxmatch))
		{
			print(paste('idxmatch[x] is',idxmatch[x]))
			print(paste('setFlag[idxmatch[x]] is',setFlag[idxmatch[x]]))
			if(setFlag[idxmatch[x]]==1)
				next
			print(paste('idxmatch[x] is',idxmatch[x]))
			setFlag[idxmatch[x]] = 1
			toprint = 1
			print(paste('Sending error for',botname[idxmatch[x]]))
			sendMailError(botname[idxmatch[x]],"DOWN")
		}
	}
		Sys.sleep(3600)
		monitorBotsStuck()
}
sink()
