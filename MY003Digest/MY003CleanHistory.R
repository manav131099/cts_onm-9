rm(list = ls())
errHandle = file('/home/admin/Logs/LogsMY003History.txt',open='w',encoding='UTF-8')
sink(errHandle,type='message',append = T)
sink(errHandle,type='output',append = T)
require('mailR')
source('/home/admin/CODE/MY003Digest/FTPMY003Dump.R')
source('/home/admin/CODE/Misc/memManage.R')
source('/home/admin/CODE/Send_mail/sendmail.R')
FIREERRATA = c(1,1,1)
LTCUTOFF = 0.001
checkdir = function(x)
{
  if(!file.exists(x))
  {
    dir.create(x)
  }
}

timetomins = function(x)
{
  hrs = unlist(strsplit(x,":"))
  mins = as.numeric(hrs[2])
  hrs = as.numeric(hrs[1]) * 60
  return((((hrs + mins))+1))
}

checkErrata = function(row,ts,no)
{
	{
  if(no == 1)
	  no2 = 'GF-1'
	else if(no == 2)
	  no2 = 'GF-2'
	else if(no==3)
		no2="GF-3"
	}
  if(ts < 540 || ts > 1020)
	{return()}
	if(FIREERRATA[no] == 0)
	{
	  print(paste('Errata mail already sent for meter',no,'so no more mails'))
		return()
	}
	if((!(is.finite(as.numeric(row[2])))) || (as.numeric(row[2]) < LTCUTOFF))
	{
		FIREERRATA[no] <<- 0
		subj = paste('MY-003X',no2,'down')
    body = 'Last recorded timestamp and reading\n'
		body = paste(body,'Timestamp:',as.character(row[1]))
		body = paste(body,'Real Power Tot kW reading:',as.character(row[2]))
		send.mail(from = 'operations@cleantechsolar.com',
							to = getRecipients("MY-003X","a"),
							subject = subj,
							body = body,
							smtp = list(host.name = "smtp.office365.com", port = 587, user.name = 'shravan.karthik@cleantechsolar.com',passwd = 'CTS&*(789', tls = TRUE),
							authenticate = TRUE,
							send = TRUE,
							debug = F)
		print(paste('Errata mail sent meter ',no))
		msgbody = paste(subj,"\n Timestamp:",as.character(row[1]),
		                "\n Real Pow Tot kW reading:",as.character(row[2]))
		command = paste('python /home/admin/CODE/Send_mail/twilio_alert.py',' "',msgbody,'"',' "MY-003X"',sep = "")    ########## CHANGE PYTHON SCRIPT TO REFLECT TRUE OWNERS AS ARUN IS REMOVED
		system(command)
		recordTimeMaster("MY-003X","TwilioAlert",as.character(row[1]))
		print('Twilio message fired')
	}
}

stitchFile = function(path,days,pathwrite,erratacheck)
{
x = 1
t = try(read.csv(paste(path,days[x],sep="/"),header = T, skip=6,stringsAsFactors=F),silent = T)
  if(length(t)==1)
        {
                print(paste("Skipping",days[x],"Due to err in file"))
                return()
        }
  dataread = t
        if(nrow(dataread) < 1)
                return()
  dataread = dataread[,-c(1,2)]
  currcolnames = colnames(dataread)
  idxvals = match(currcolnames,colnames)
  idxvals = idxvals[complete.cases(idxvals)]
	if(length(idxvals) < 1)
	return()
	#temp = unlist(strsplit(days[x]," "))
  #      temp = unlist(strsplit(temp[2],"_"))
  #temp = as.numeric(temp[1])a
	tmstmps = as.character(dataread[,1])
	tmstmps = unlist(strsplit(tmstmps," "))
	seqa = seq(from =1 ,to=length(tmstmps),by=2)
	tmstmps2 = tmstmps[seqa]
	tmstmps = tmstmps[(seqa+1)]
	tmstmps3 = unlist(strsplit(tmstmps2,"-"))
	seqb = seq(from=1,to=length(tmstmps3),by=3)
	tmstmps3 = paste(tmstmps3[(seqb+2)],tmstmps3[(seqb)],tmstmps3[(seqb+1)],sep="-")
	tmstmps = paste(tmstmps3,tmstmps)
	dataread[,1]=as.character(tmstmps)
	temp = substr(days[x],10,10)
	{
	if(temp=='1')
		temp = 1
	else if(temp=='2')
		temp=2
	else if(temp=='3')
		temp=3
	}
  for(y in 1 : nrow(dataread))
  {
    idxts = timetomins(substr(as.character(dataread[y,1]),12,16))
    if(!is.finite(idxts))
    {
      print(paste('Skipping row',y,'as incorrect timestamp'))
      next
    }
    rowtemp2 = rowtemp
    yr = substr(as.character(dataread[y,1]),1,4)
    pathwriteyr = paste(pathwrite,yr,sep="/")
    checkdir(pathwriteyr)
    mon = substr(as.character(dataread[y,1]),1,7)
    pathwritemon = paste(pathwriteyr,mon,sep="/")
    checkdir(pathwritemon)
		{
		if(temp==1)
		  temp2 = 'GF-1'
		else if(temp == 2)
		  temp2 = 'GF-2'
		else if(temp==3)
			temp2='GF-3'
    }
		pathwritefinal = paste(pathwritemon,temp2,sep="/")
    checkdir(pathwritefinal)
    mtno = temp
    day = substr(as.character(dataread[y,1]),1,10)
    filename = paste(stnname,temp,"] ",day,".txt",sep="")
    pathtowrite = paste(pathwritefinal,filename,sep="/")
    rowtemp2[idxvals] = dataread[y,]
		dfsanityCheck = try(read.table(pathtowrite,header =T,sep = "\t",
		stringsAsFactors=F),silent=T) 
		{
    if(!file.exists(pathtowrite) || class(dfsanityCheck)=="try-error")
    {
      df = data.frame(rowtemp2,stringsAsFactors = F)
      colnames(df) = colnames
      if(idxts != 1)
        {
          df[idxts,] = rowtemp2
          df[1,] = rowtemp
        }
			FIREERRATA <<- c(1,1,1)
    }
    else
    {
      df = read.table(pathtowrite,header =T,sep = "\t",stringsAsFactors=F)
      df[idxts,] = rowtemp2
    }
  }
	if(erratacheck != 0)
	{
	idxEac1 = 15
	pass = c(as.character(df[idxts,1]),as.character(df[idxts,idxEac1]))
	checkErrata(pass,idxts,mtno)
	}
	df = df[complete.cases(df[,1]),]
	tdx = as.POSIXct(strptime(as.character(df[,1]), "%Y-%m-%d %H:%M:%S"))
	df = df[order(tdx),]
  write.table(df,file = pathtowrite,row.names = F,col.names = T,sep = "\t",append = F)
  }
	recordTimeMaster("MY-003X","Gen1",as.character(df[nrow(df),1]))
}

path = '/home/admin/Data/Episcript-CEC/MY003 Dump'
pathwrite = '/home/admin/Dropbox/Gen 1 Data/[MY-003X]'
pathdatelastread = '/home/admin/Start'
checkdir(pathdatelastread)
pathdatelastread = paste(pathdatelastread,'MY003.txt',sep="/")
days = dir(path)
days = days[grepl("csv",days)]
lastrecordeddate = c('','','')
idxtostart = c(1,2,3)
{
	if(!file.exists(pathdatelastread))
	{
		print('Last date read file not found so cleansing system from start')
		system('rm -R "/home/admin/Dropbox/Gen 1 Data/[MY-003X]"')
	}
	else
	{
		lastrecordeddate = readLines(pathdatelastread)
		lastrecordeddate = c(lastrecordeddate[1],lastrecordeddate[2],lastrecordeddate[3])
		idxtostart[1] = match(lastrecordeddate[1],days)
		idxtostart[2] = match(lastrecordeddate[2],days)
		idxtostart[3] = match(lastrecordeddate[3],days)
		print(paste('Date read is',lastrecordeddate[1],'and idxtostart is',idxtostart[1]))
		print(paste('Date read is',lastrecordeddate[2],'and idxtostart is',idxtostart[2]))
		print(paste('Date read is',lastrecordeddate[3],'and idxtostart is',idxtostart[3]))
	}
}
checkdir(pathwrite)
colnames = colnames(read.csv(paste(path,"MY-003-GF1_20180920000000.csv",sep="/"),header = T, skip=6,stringsAsFactors=T))
colnames = colnames[-c(1,2)]
rowtemp = rep(NA,(length(colnames)))
x=1
stnname =  "[MY-003X-M"
days1 = days[grepl('MY-003-GF1',days)]
days2 = days[grepl('MY-003-GF2',days)]
days3 = days[grepl('MY-003-GF3',days)]
idxtostart[2] = idxtostart[2] - length(days1)
idxtostart[3] = idxtostart[3] - length(days1) - length(days2)
{
	if(idxtostart[1] < length(days1))
	{
		print(paste('idxtostart[1] is',idxtostart[1],'while length of days1 is',length(days1)))
		for(x in idxtostart[1] : length(days1))
		{
			print(days1[x])
 		 stitchFile(path,days1[x],pathwrite,0)
		}
		lastrecordeddate[1] = as.character(days1[x])
		print('Meter 1 DONE')
	}
	if(idxtostart[2] < length(days2))
	{
		print(paste('idxtostart[2] is',idxtostart[2],'while length of days2 is',length(days2)))
		for(x in idxtostart[2] : length(days2))
		{
			print(days2[x])
 		 stitchFile(path,days2[x],pathwrite,0)
		}
		lastrecordeddate[2] = as.character(days2[x])
		print('Meter 2 Done')
	}
	if(idxtostart[3] < length(days3))
	{
		print(paste('idxtostart[3] is',idxtostart[3],'while length of days3 is',length(days3)))
		for(x in idxtostart[3] : length(days3))
		{
			print(days3[x])
 		 stitchFile(path,days3[x],pathwrite,0)
		}
		lastrecordeddate[3] = as.character(days3[x])
		print('Meter 2 Done')
	}
	
	else if( (!(idxtostart[1] < length(days1))) && (!(idxtostart[2] < length(days2))) && (!(idxtostart[3])<length(days3)))
	{
		print('No historical backlog')
	}
}

print('_________________________________________')
print('Historical Calculations done')
print('_________________________________________')
reprint = 1
print(lastrecordeddate)
write(lastrecordeddate,pathdatelastread)
print('File Written')
ERRATAFIX=0
while(1)
{
  print('Checking FTP for new data')
  filefetch = dumpftp(days,path)
	if(filefetch == 0)
	{
		Sys.sleep(600)
		next
	}
	print('FTP Check complete')
	daysnew = dir(path)
	daysnew = daysnew[grepl("csv",daysnew)]
	if(length(daysnew) == length(days))
	{
	  if(reprint == 1)
		{
			reprint = 0
			print('Waiting for new file dump')
		}
		Sys.sleep(600)
		next
	}
	reprint = 1
	match = match(days,daysnew)
	days = daysnew
	daysnew = daysnew[-match]
	for(x in 1 : length(daysnew))
	{
	  print(paste('Processing',daysnew[x]))
		stitchFile(path,daysnew[x],pathwrite,ERRATAFIX)
		print(paste('Done',daysnew[x]))
	}
	daysnew1 = daysnew[grepl('MY-003-GF1',daysnew)]
	daysnew2 = daysnew[grepl('MY-003-GF2',daysnew)]
	daysnew3 = daysnew[grepl('MY-003-GF3',daysnew)]

	if(length(daysnew1) < 1)
		daysnew1 = lastrecordeddate[1]
	if(length(daysnew2) < 1)
		daysnew2 = lastrecordeddate[2]
	if(length(daysnew3) < 1)
		daysnew3 = lastrecordeddate[3]

	write(c(as.character(daysnew1[length(daysnew1)]),as.character(daysnew2[length(daysnew2)]),
	as.character(daysnew3[length(daysnew3)])),pathdatelastread)
	lastrecordeddate[1] = as.character(daysnew1[length(daysnew1)])
	lastrecordeddate[2] = as.character(daysnew2[length(daysnew2)])
	lastrecordeddate[3] = as.character(daysnew3[length(daysnew3)])
gc()
ERRATAFIX=0 #for now once GSI reading is fine change this to 1
}
print('Out of loop')
sink()
