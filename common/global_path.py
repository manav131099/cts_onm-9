#Master Path
GLOB_HOME = '/home/admin'

#Start file path
GLOB_START = GLOB_HOME + '/Start'

#Code Location
GLOB_CODE = GLOB_HOME + '/CODE'
GLOB_DBCREATION = GLOB_CODE + '/DatabaseCreation'

#Flexi Raw Path
GLOB_DATA = GLOB_HOME + '/Data'
GLOB_FLEXI_RAW = GLOB_DATA + '/Flexi_Raw_Data'

#Flexi Gen1 Path
GLOB_DROPBOX = GLOB_HOME + '/Dropbox'
GLOB_DROPBOX_FLEXI = GLOB_DROPBOX + '/FlexiMC_Data/'
GLOB_FLEXI_GEN1 = GLOB_DROPBOX_FLEXI + 'Gen1_Data'

#Mail
GLOB_FROM_ADDR = 'operations@cleantechsolar.com'
GLOB_UNAME = 'shravan.karthik@cleantechsolar.com'
GLOB_PWD = 'CTS&*(789'

#Flexi Server
GLOB_FLEXI_SERVER = 'https://fbapi.fleximc.com/API/JSON/CleanTech'
