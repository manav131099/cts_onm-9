from flask import Flask, render_template, request, Response, jsonify
import requests
import pandas as pd
import itertools
from numpy.random import randint
import datetime
import re
import ast
import pandas as pd
import logging
import pandas as pd
import numpy as np
import json
import werkzeug._internal
import os
import pyodbc

def demi_logger(type, message,*args,**kwargs):
    pass

path='/home/admin/Dropbox/Lifetime/Gen-1/'
server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 13 for SQL Server}'

cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
SQL_Query = pd.read_sql_query("SELECT * FROM [dbo].[Users]", cnxn)
df_users = pd.DataFrame(SQL_Query, columns=['User_Id','Username','Password'])
users=pd.Series(df_users['User_Id'].values,index=df_users['Password']).to_dict()
SQL_Query = pd.read_sql_query("SELECT [dbo].[Meters].[Station_Id],[Meter_id],[Country_Id],[Capacity],[Station_Name] FROM [dbo].[Meters] INNER JOIN [dbo].[Stations] ON [dbo].[Stations].[Station_Id] = [dbo].[Meters].[Station_Id]", cnxn)
df_meterinfo = pd.DataFrame(SQL_Query, columns=['Station_Id','Meter_id','Country_Id','Capacity','Station_Name'])
cnxn.close()

def update_azure(stn,df,Type):
    server = 'cleantechsolar.database.windows.net'
    database = 'Cleantech Meter Readings'
    username = 'RohanKN'
    password = 'R@h@nKN1'
    driver= '{ODBC Driver 13 for SQL Server}'
    df.fillna('NULL')
    cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
    meter_vals=df.values.tolist()[0]
    print(meter_vals)
    cursor = cnxn.cursor()
    date=meter_vals[0]
    ghi=meter_vals[1]
    ghi_flag=meter_vals[2]
    master_flag=float(meter_vals[-1])
    print(stn)
    df_meterid=df_meterinfo.loc[df_meterinfo['Station_Name'].str.strip()==stn,['Station_Id','Meter_id','Country_Id','Capacity']]
    print(df_meterid)
    for index,i in enumerate(df_meterid.values.tolist()):
        lastr=float(meter_vals[3+2*index])
        lastr_flag=float(meter_vals[4+2*index])
        eac=float(meter_vals[3+len(df_meterid)*2+2*index])
        eac_flag=float(meter_vals[2+2*len(df_meterid)+(2*(index+1))])
        yld=float(meter_vals[3+len(df_meterid)*2+2*index])/float(i[3])
        print(meter_vals[1])
        if((meter_vals[1])=='' or float(meter_vals[1])==0):
            pr=0
        elif(float(meter_vals[1])!=0):
            pr=((float(meter_vals[3+len(df_meterid)*2+2*index])/float(i[3]))/float(meter_vals[1]))*100
        if(Type=='insert'):
            try:
                print('Inserting',master_flag)
                with cursor.execute("INSERT INTO dbo.Stations_Data([Date],[GHI],[GHI-Flag],[LastR-MFM],[LastR-Flag],[Eac-MFM],[Eac-Flag],[Station_Id],[Meter_Id],[Country_Id],[Yield],[PR],[Master-Flag]) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", date, ghi, ghi_flag, lastr, lastr_flag, eac, eac_flag, i[0], i[1], i[2], yld, pr,master_flag):
                    pass
                cnxn.commit()
            except Exception as e:
                print(e)
                pass
        else:
            print('Updating',master_flag)
            try:
                with cursor.execute("UPDATE [dbo].[Stations_Data] SET [GHI]="+str(ghi)+",[GHI-Flag]="+str(ghi_flag)+",[LastR-MFM]="+str(lastr)+",[LastR-Flag]="+str(lastr_flag)+",[Eac-MFM]="+str(eac)+",[Eac-Flag]="+str(eac_flag)+",[Yield]="+str(yld)+",[PR]="+str(pr)+",[Master-Flag]="+str(master_flag)+" WHERE [Date]='"+str(date)+"' and [Meter_Id]="+str(i[1])):
                    pass
                cnxn.commit()
            except Exception as e:
                print(e)
                pass
    cnxn.close()

def check_float(n):
    try:
        float(n)
        return True
    except:
        print(n)
        return False
        
# df_tags
app = Flask(__name__)

@app.route('/')
def index():
    return render_template('Web_Interface_V2.html')


@app.route('/get_data',methods=['POST'])
def get_data():
    info = request.get_json(force=True)
    key=info['Key']
    print(key)
    if(key in users):
        stn=info['Id']
        date=info['Date']
        print(info)
        if(os.path.exists(path+stn+'-LT.txt')):
            df=pd.read_csv(path+stn+'-LT.txt',sep='\t')
            if(stn=='IN-050'):
                columns=df.columns.tolist()[1:]#No date. Does not have Last,DA.
            else:
                columns=df.columns.tolist()[1:-3]+[df.columns.tolist()[-1]]#No date,LastT,DA
            df_temp=df.loc[df['Date']==date,columns]
            if df_temp.empty:
                df_temp.loc[len(df_temp)] = 0
            df_temp=df_temp.fillna(0)
            df_temp['Master-Flag']=users[key]
            print(df_temp.to_json(orient="records"))
            return Response(df_temp.to_json(orient="records"),status=200)
        else:
            return jsonify(['Site not automated']) 
    else:
        return jsonify(['Incorrect Secret Key'])


@app.route('/update_data',methods=['POST'])
def update_data():
    data = request.form.to_dict()
    print(data)
    if data['Date']=='' or data['Code']=='':
        return jsonify(['Failed'])
    df_temp=pd.DataFrame(data,index=[0])
    if(data['Code']=='IN-050'):
        print('IN-050')
    else:
        df_temp=df_temp.iloc[:,1:]
    df=pd.read_csv(path+data['Code']+'-LT.txt',sep='\t')
    if(data['Code']=='IN-050'):
        columns=df.columns.tolist()#Does not have Last,DA.
    else:
        columns=df.columns.tolist()[0:-3]+[df.columns.tolist()[-1]]#No date,LastT,DA
    df_temp=df_temp[columns] #reorder and also gets rid of O&M Code
    for i in df_temp.values[0][1:]:
        if(check_float(i)):
            pass
        else:
          print(i)
          return jsonify(['Entered invalid value. Please try again.'])
    df_check=df.loc[df['Date']==data['Date'],columns]
    if df_check.empty:
        print('empty')
        df_temp.insert(len(columns)-1,'DA',0)
        df_temp.insert(len(columns)-1,'LastT',np.nan)
        print(df_temp)
        print(df_temp.columns)
        df_temp.to_csv(path+data['Code']+'-LT.txt',sep='\t',index=False,mode='a',header=False)
        update_azure(data['Code'],df_temp,'insert')
        return jsonify(['Success'])
    else:
        print(df_temp)
        df.loc[df['Date']==data['Date'],columns]=df_temp.values.tolist()[0]
        df.to_csv(path+data['Code']+'-LT.txt',sep='\t',index=False)
        update_azure(data['Code'],df_temp,'update')
        return jsonify(['Success'])

if __name__ == '__main__':
    werkzeug._internal._log = demi_logger
    app.run(host = '0.0.0.0',port=5000,debug=True)