import os
import re
import pandas as pd
import datetime
import os
import time
import pytz
import pyodbc
from twilio.rest import TwilioRestClient
import math
import numpy as np
import logging

server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 13 for SQL Server}'

startpath="/home/pranav/"
mailpath='/home/admin/CODE/Send_mail/mail_recipients.csv'
smspath='/home/admin/CODE/Send_mail/alerts_recipients.csv'

tz = pytz.timezone('Asia/Singapore')
tz2=pytz.timezone('Asia/Phnom_Penh')
tz3=pytz.timezone('Asia/Calcutta')
tz4=pytz.timezone('Asia/Bangkok')
tz5=pytz.timezone('Asia/Ho_Chi_Minh')

start=datetime.datetime.now(tz).date()
timenow1=datetime.datetime.now(tz)

starttime=time.time()


table=[['SG-003S-L','SG-005S-L','KH-008S-L','MY-006S-L','IN-015S-L','IN-036S-L','MY-004S-L','VN-001S-L'],['716',"'724,725,726,727'",'728','729','712','718','720','722'],['Asia/Singapore','Asia/Singapore','Asia/Phnom_Penh','Asia/Malaysia','Asia/Calcutta','Asia/Calcutta','Asia/Singapore','Asia/Ho_Chi_Minh'],['SERIS','SERIS','SERIS','SERIS','SERIS','SERIS','SERIS','SERIS']]
columns=['[Eac_A],[Eac_B],[Eac_C]','[Act_E-Del_F3-5],[Act_E-Del_F2],[Act_E-Del_F7A],[Act_E-Del_F7B],[Act_E-Del_F7G]','[Act_E-Del_Pond],[Act_E-Del_R06],[Act_E-Del_R02],[Act_E-Del_R03],[Act_E-Del_R11]','[Act_E-Del_M1],[Act_E-Del_M2],[Act_E-Del_M3]','[Act_E-Del-Rec_LV],[Act_E-Del-Rec_HV]','[Act_E-Del-Rec_TF_STR],[Act_E-Del-Rec_TF_CTR]','[Act_E-Recv_M1]','[Act_E-Del_M1]']
alert=[[0,0,0,0,0,0,0,0],[timenow1,timenow1,timenow1,timenow1,timenow1,timenow1,timenow1,timenow1],[0,0,0,0,0,0,0,0],[timenow1,timenow1,timenow1,timenow1,timenow1,timenow1,timenow1,timenow1],[0,0,0,0,0,0,0,0],[timenow1,timenow1,timenow1,timenow1,timenow1,timenow1,timenow1,timenow1]]
exclude=['','','[Act_E-Del_Pond],','','','','','']
columns2=[]
irrcolumns=['Gsi00','AvgGsi00','AvgGsi00_Shr','AvgSMP10','AvgGsi00','AvgGTI_CTC','AvgGsi00','AvgGsi00']
for i in range(len(columns)):
    columns2.append(columns[i].replace(exclude[i],''))

def delete():
    cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    with cursor.execute("IF OBJECT_ID('[Cleantech Meter Readings].dbo.[Seris_Summary_All2]', 'U') IS NOT NULL TRUNCATE TABLE [Cleantech Meter Readings].dbo.[Seris_Summary_All2]"): 
        print('Successfuly Deleted cause already present!') 

def rectolist(station,path):
    df=pd.read_csv(path)
    df2=df[['Recipients',station]]
    a=df2.loc[df2[station] == 1]['Recipients'].tolist()
    return a

def sendmeteralarm(a,meternames,data2,rec,smsrec,type):
    SERVER = "smtp.office365.com"
    FROM = 'operations@cleantechsolar.com'
    #recipients = rec # must be a list
    recipients=rec
    recipients.append('sai.pranav@cleantechsolar.com')
    TO=", ".join(recipients)
    SUBJECT = a
    text2='Last Timestamp Read: '+str(data2) +'\n\n'
    if(type==1):
        text2=text2+'Meters Stuck: \n\n'
    elif(type==2):
        text2=text2+'Meters with NA values: \n\n'
    for i in meternames:
        text2=text2+i+'\n\n'
    TEXT = text2
    # Prepare actual message
    message = "From:"+FROM+"\nTo:"+TO+"\nSubject:"+ SUBJECT +"\n\n"+TEXT
    # Send the mail
    import smtplib
    server = smtplib.SMTP(SERVER)
    server.starttls()
    server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
    server.sendmail(FROM, recipients, message)
    server.quit()
    account = "ACcac80891fc3a1edb14f942975a0a8939"
    token = "d39acf2278b2194b59aa15aa9e3a2072"
    client = TwilioRestClient(account, token)
    for t in smsrec:
        message = client.messages.create(to="+"+str(int(t)), from_="+12402930809",body=SUBJECT +"\n\n"+TEXT)

def sendtimestampmail(a,data,data2,col,rec,smsrec):
    SERVER = "smtp.office365.com"
    FROM = 'operations@cleantechsolar.com'
    recipients=rec
    recipients.append('sai.pranav@cleantechsolar.com')
    TO=", ".join(recipients)
    SUBJECT = a
    text2='Last Timestamp Read: '+str(data2) +'\n\n'
    col=col.split(',')
    for i in range(len(data)):
        if(data[i]==None):
            text2=text2+col[i]+' Last Meter Reading: NA\n\n'
        else:
            text2=text2+col[i]+' Last Meter Reading: '+str(data[i])+'\n\n'
    print(text2)
    TEXT = text2
    # Prepare actual message
    message = "From:"+FROM+"\nTo:"+TO+"\nSubject:"+ SUBJECT +"\n\n"+TEXT
    # Send the mail
    import smtplib
    server = smtplib.SMTP(SERVER)
    server.starttls()
    server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
    server.sendmail(FROM, recipients, message)
    server.quit()
    account = "ACcac80891fc3a1edb14f942975a0a8939"
    token = "d39acf2278b2194b59aa15aa9e3a2072"
    client = TwilioRestClient(account, token)
    for t in smsrec:
        message = client.messages.create(to="+"+str(int(t)), from_="+12402930809",body=SUBJECT +"\n\n"+TEXT)

#Insert Function
def insert(table):
    value3=0
    counter=0
    while True:            
        try:
            cnxn = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)
            cursor = cnxn.cursor()
            #Dumping data in the new table

            for i in range(len(table[0])):
                #Meter Flag
                print(table[0][i])
                if(table[0][i]=='TH-002X' or table[0][i]=='VN-001S-L' or table[0][i]=='MY-004S-L'):
                    cursor.execute("SELECT TOP(1) "+columns2[i]+"  FROM dbo.["+table[0][i]+"] ORDER BY [Timestamp] DESC;")
                else:
                    cursor.execute("SELECT TOP(1) "+columns2[i]+"  FROM dbo.["+table[0][i]+"] ORDER BY [Time Stamp] DESC;")
                data1 = cursor.fetchone()
                value=0
                for j in data1:
                    if(j==None or j<0 or j=='NA' or j=='N/A'):
                        value=1  
                print(value)   
                #Timestamp Flag
                if(table[0][i]=='TH-002X' or table[0][i]=='VN-001S-L' or table[0][i]=='MY-004S-L'):
                    cursor.execute("SELECT TOP(1) [Timestamp] FROM dbo.["+table[0][i]+"] ORDER BY [Timestamp] DESC;")
                else:
                    cursor.execute("SELECT TOP(1) [Time Stamp] FROM dbo.["+table[0][i]+"] ORDER BY [Time Stamp] DESC;")
                data = cursor.fetchone()
                data=data[0]
                print(data)
                data= datetime.datetime.strptime(str(data), '%Y-%m-%d %H:%M:%S')
                if(table[0][i]=='KH-008S-L'):
                    timenow=datetime.datetime.now(tz2)
                    timenow=timenow.replace(tzinfo=None)
                elif(table[0][i]=='IN-015S-L' or table[0][i]=='IN-036S-L'):
                    timenow=datetime.datetime.now(tz3)
                    timenow=timenow.replace(tzinfo=None)
                elif(table[0][i]=='TH-002X'):
                    timenow=datetime.datetime.now(tz4)
                    timenow=timenow.replace(tzinfo=None)
                elif(table[0][i]=='VN-001S-L'):
                    timenow=datetime.datetime.now(tz4)
                    timenow=timenow.replace(tzinfo=None)
                else:
                    timenow=datetime.datetime.now(tz)
                    timenow=timenow.replace(tzinfo=None)
                duration = timenow - data
                print(duration.seconds//60)
                if(table[0][i]=='TH-002X'):
                    mintime=200
                    mintime2=200
                elif(table[0][i]=='IN-036S-L' or table[0][i]=='IN-015S-L'):
                    mintime=30
                    mintime2=240
                elif(table[0][i]=='SG-005S-L'):
                    mintime=30
                    mintime2=60
                else:
                    mintime=15
                    mintime2=60
                if(table[0][i]=='KH-008S-L'):
                    minhours=16
                    minhours2=8
                elif(table[0][i]=='TH-002X'):
                    minhours=17
                    minhours2=10
                else:
                    minhours=17
                    minhours2=8
                #Meter Alarms   
                if(table[0][i]=='TH-002X' or table[0][i]=='VN-001S-L' or table[0][i]=='MY-004S-L'):
                    cursor.execute("SELECT TOP(1) "+ irrcolumns[i] +" FROM dbo.["+table[0][i]+"] ORDER BY [Timestamp] DESC;")
                else:
                    cursor.execute("SELECT TOP(1) "+ irrcolumns[i] +" FROM dbo.["+table[0][i]+"] ORDER BY [Time Stamp] DESC;")
                irrdata = cursor.fetchone()
                irrdata= irrdata[0]
                print('irr data is',irrdata)
                if(timenow.hour>=minhours2 and timenow.hour<=minhours and irrdata>30):
                    if(table[0][i]=='TH-002X' or table[0][i]=='VN-001S-L' or table[0][i]=='MY-004S-L'):
                        cursor.execute("SELECT TOP(20) "+columns2[i]+" FROM dbo.["+table[0][i]+"] ORDER BY [Timestamp] DESC;")
                    else:
                        cursor.execute("SELECT TOP(10) "+columns2[i]+" FROM dbo.["+table[0][i]+"] ORDER BY [Time Stamp] DESC;")
                    data5=cursor.fetchall()
                    for p in range(0,len(data5)):
                        data5[p]=tuple(data5[p])
                    df5 = pd.DataFrame(data5, columns =columns2[i].split(','))
                    send=0
                    send2=0
                    temp=[]
                    temp2=[]
                    print(df5)
                    if(table[0][i]=='TH-002X'):
                        df5=df5.replace('NA',np.NaN)
                        df5=df5.replace('N/A',np.NaN)
                        df5= df5.astype(float)
                    for k in df5.columns:
                        print("Std is",df5[k].std())
                        if(df5[k].std()==0):
                            send=1
                            temp.append(k)
                        if(math.isnan(df5[k].std())):
                            send2=1
                            temp2.append(k)

                    #Meter NA Alarm  
                    if(send2==1 and (table[0][i]!='IN-036S-L' and table[0][i]!='TH-002X')):
                        if(alert[4][i]==0):
                            alert[4][i]=1
                            alert[5][i]=datetime.datetime.now(tz)
                            recipients=rectolist(table[0][i][0:7]+'_Mail',mailpath)
                            smsrecipients=rectolist(table[0][i][0:7],smspath)
                            print('SENDING THE ALARM!')
                            sendmeteralarm(table[0][i]+ " Meter NA Alarm",temp2,data,recipients,smsrecipients,2)
                        elif(alert[4][i]==1):
                            diff=datetime.datetime.now(tz)-alert[5][i]
                            if((diff.seconds)//60>mintime2):
                                alert[5][i]=datetime.datetime.now(tz)
                                recipients=rectolist(table[0][i][0:7]+'_Mail',mailpath)
                                smsrecipients=rectolist(table[0][i][0:7],smspath)
                                sendmeteralarm(table[0][i]+ " Meter NA Alarm",temp2,data,recipients,smsrecipients,2)
                        with open(startpath+table[0][i]+"MeterAlarm.txt", "w") as file:
                            file.write(str(timenow)) 
                    else:
                        alert[4][i]==0

                    #Meter Stuck Alarm  
                    if(send==1 and (table[0][i]!='IN-036S-L' and table[0][i]!='TH-002X')):
                        if(alert[2][i]==0):
                            alert[2][i]=1
                            alert[3][i]=datetime.datetime.now(tz)
                            recipients=rectolist(table[0][i][0:7]+'_Mail',mailpath)
                            smsrecipients=rectolist(table[0][i][0:7],smspath)
                            sendmeteralarm(table[0][i]+ " Meter Stuck Alarm",temp,data,recipients,smsrecipients,1)
                        elif(alert[2][i]==1):
                            diff=datetime.datetime.now(tz)-alert[3][i]
                            if((diff.seconds)//60>mintime2):
                                alert[3][i]=datetime.datetime.now(tz)
                                recipients=rectolist(table[0][i][0:7]+'_Mail',mailpath)
                                smsrecipients=rectolist(table[0][i][0:7],smspath)
                                sendmeteralarm(table[0][i]+ " Meter Stuck Alarm",temp,data,recipients,smsrecipients,1)
                        with open(startpath+table[0][i]+"MeterAlarm.txt", "w") as file:
                            file.write(str(timenow)) 
                        value3=1
                    else:
                        alert[2][i]==0
                        value3=0
    
                #Network Issue Alarm          
                if((duration.seconds//60)>mintime and (table[0][i]!='IN-036S-L' and table[0][i]!='TH-002X')):
                    if(timenow.hour>=minhours2 and timenow.hour<=minhours):
                        if(alert[0][i]==0):
                            alert[0][i]=1
                            alert[1][i]=datetime.datetime.now(tz)
                            recipients=rectolist(table[0][i][0:7]+'_Mail',mailpath)
                            smsrecipients=rectolist(table[0][i][0:7],smspath)
                            sendtimestampmail(table[0][i]+ " Network Issue Alarm",data1,data,columns[i],recipients,smsrecipients)
                        elif(alert[0][i]==1):
                            diff=datetime.datetime.now(tz)-alert[1][i]
                            if((diff.seconds)//60>mintime2):
                                alert[1][i]=datetime.datetime.now(tz)
                                recipients=rectolist(table[0][i][0:7]+'_Mail',mailpath)
                                smsrecipients=rectolist(table[0][i][0:7],smspath)
                                sendtimestampmail(table[0][i]+ " Network Issue Alarm",data1,data,columns[i],recipients,smsrecipients)
                        with open(startpath+table[0][i]+".txt", "w") as file:
                            file.write(str(timenow)) 
                    value2=1
                else:
                    alert[0][i]=0
                    value2=0
                print(table[0][i][0:6])
                print(table[2][i])
                print(table[3][i])
                print(("INSERT INTO [dbo].[Seris_Summary_All2] ([Time Stamp],[StationId],[StationName],[Time Zone],[Meter_Flag],[TimeStamp_Flag],[MeterStuck_Flag],[Monitoring Provider]) SELECT TOP(1) [Timestamp]," + table[1][i] +",'"+table[0][i][0:6]+ "','" + table[2][i]+ "'," + str(value)+ ',' +str(value2) +',' +str(value3) + ",'" + table[3][i] + "' FROM [dbo].["+table[0][i]+"] ORDER BY [Timestamp] DESC;"))
                if(table[0][i]=='TH-002X' or table[0][i]=='VN-001S-L' or table[0][i]=='MY-004S-L'):
                    with cursor.execute("INSERT INTO [dbo].[Seris_Summary_All2] ([Time Stamp],[StationId],[StationName],[Time Zone],[Meter_Flag],[TimeStamp_Flag],[MeterStuck_Flag],[Monitoring Provider]) SELECT TOP(1) [Timestamp]," + table[1][i] +",'"+table[0][i][0:6]+ "','" + table[2][i]+ "'," + str(value)+ ',' +str(value2) +',' +str(value3) + ",'" + table[3][i] + "' FROM [dbo].["+table[0][i]+"] ORDER BY [Timestamp] DESC;"): 
                        print('Successfuly Inserted Row!') 
                else:
                    with cursor.execute("INSERT INTO [dbo].[Seris_Summary_All2] ([Time Stamp],[StationId],[StationName],[Time Zone],[Meter_Flag],[TimeStamp_Flag],[MeterStuck_Flag],[Monitoring Provider]) SELECT TOP(1) [Time Stamp]," + table[1][i] +",'"+table[0][i][0:6]+ "','" + table[2][i]+ "'," + str(value)+ ',' +str(value2)+ ',' +str(value3) + ",'" + table[3][i] + "' FROM [dbo].["+table[0][i]+"] ORDER BY [Time Stamp] DESC;"): 
                        print('Successfuly Inserted Row!') 
            cnxn.commit()
            cursor.close()
            cnxn.close()
            break 
        except Exception as e:
            print(e)
            logging.exception('msg')
            cursor.close()
            cnxn.close()
            if err_num == '08S01':
                counter = counter + 1
                print ('Sleeping for 5')
                time.sleep(300)
                if counter > 10:
                    exit('Persistent connection error')                    
                print ('%%%Re-trying connection%%%')             
            else:
                pass

while(1):
    delete()
    insert(table)
    time.sleep(60.0 - ((time.time() - starttime) % 60.0))
