import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import ast
import pyodbc
from twilio.rest import TwilioRestClient

path='/home/admin/Dropbox/FlexiMC_Data/Gen1_Data/[IN-070C]/'
mailpath='/home/admin/CODE/Send_mail/mail_recipients.csv'
tz=pytz.timezone('Asia/Kolkata')
meters=['ESS_2','ESS_3','ESS_10']
def sendmail(station,meterdata,data,rec):
    SERVER = "smtp.office365.com"
    FROM = 'operations@cleantechsolar.com'
    recipients = rec# must be a list
    #recipients.append('sai.pranav@cleantechsolar.com')
    TO=", ".join(recipients)
    SUBJECT = station+' Power Trip Alarm'
    text2='Last Timestamp Read: '+str(data) +'\n\n'+'Meters with issues:'+','.join(meterdata)+'\n\n'
    TEXT = text2
    # Prepare actual message
    message = "From:"+FROM+"\nTo:"+TO+"\nSubject:"+ SUBJECT +"\n\n"+TEXT
    # Send the mail
    import smtplib
    server = smtplib.SMTP(SERVER)
    server.starttls()
    server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
    server.sendmail(FROM, recipients, message)
    server.quit()
    account = "ACcac80891fc3a1edb14f942975a0a8939"
    token = "d39acf2278b2194b59aa15aa9e3a2072"
    client = TwilioRestClient(account, token)
    smsrec=[+918310792948,+919047055690,+919489632011,+918760461602,+919094679266,+919962549639]
    for t in smsrec:
        message = client.messages.create(to="+"+str(int(t)), from_="+12402930809",body=SUBJECT +"\n\n"+TEXT)

def rectolist(station,path):
    df=pd.read_csv(path)
    df2=df[['Recipients',station]]
    a=df2.loc[df2[station] == 1]['Recipients'].tolist()
    return a

timestamps={}
flag=0
while(1):
    time_now=datetime.datetime.now(tz)
    print(time_now)
    issue_meters=[]
    meter_flag=0
    date_now=str(datetime.datetime.now(tz).date())
    if(flag==0):
        start=time_now
        flag=1
    if(time_now.date()>start.date()):
        timestamps={}
        start=time_now
    for index,i in enumerate(meters): 
        if(os.path.exists(path+date_now[0:4]+'/'+date_now[0:7]+'/'+i+'/[IN-070C]-'+i+'-'+date_now+'.txt')):
            df=pd.read_csv(path+date_now[0:4]+'/'+date_now[0:7]+'/'+i+'/[IN-070C]-'+i+'-'+date_now+'.txt',sep='\t')
            active_power=df[['Tstamp','AC_Power_Total']].dropna()
            active_power['Tstamp'] =  pd.to_datetime(active_power['Tstamp'])
            for index, row in active_power.iterrows():
                if((row['AC_Power_Total']<1) and (row['Tstamp'].hour>7 and row['Tstamp'].hour<18)):
                    if(i in timestamps and row['Tstamp'].hour == timestamps[i].hour):
                        print(timestamps)
                        pass
                    else:
                        timestamps[i]=row['Tstamp']
                        issue_meters.append(i)
                        meter_flag=1
        else:
            print('File not present!')
    if(meter_flag==1):
        recipients=rectolist('IN-070C_Mail',mailpath)
        sendmail('IN-070C',issue_meters,timestamps[issue_meters[0]],recipients)
    print('Sleeping!')
    time.sleep(180)


