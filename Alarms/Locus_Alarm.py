import requests, json
import requests.auth
import pandas as pd
import datetime
import os
import re 
import time
import shutil
import pytz
import sys
import pyodbc
import pymsteams
from twilio.rest import Client

path='/home/admin/Dropbox/Gen 1 Data/'
mailpath='/home/admin/CODE/Send_mail/mail_recipients.csv'
timezones={'IN':'Asia/Calcutta','VN':'Asia/Bangkok','KH':'Asia/Phnom_Penh','TH':'Asia/Bangkok'}

accessurl ="https://api.locusenergy.com/oauth/token"
clientid = "0091591f4bf5f17693e1ee22acd2ee2c"
clientsecret = "7abff4f549770bc025df5eb371ce6857"

server = 'cleantechsolar.database.windows.net'
database = 'Cleantech Meter Readings'
username = 'RohanKN'
password = 'R@h@nKN1'
driver= '{ODBC Driver 13 for SQL Server}'

connStr = pyodbc.connect('DRIVER='+driver+';PORT=1433;SERVER='+server+';PORT=1443;DATABASE='+database+';UID='+username+';PWD='+ password)

SQL_Query = pd.read_sql_query('''SELECT TOP (1000) [Station_Name],[Station_Columns],[Station_Irradiation_Center],[Station_No_Meters],[Provider],[Alarm_Status] FROM [dbo].[stations] ''', connStr)
dflifetime = pd.DataFrame(SQL_Query, columns=['Station_Name','Station_Columns','Station_Irradiation_Center','Station_No_Meters','Provider','Alarm_Status'])

SQL_Query = pd.read_sql_query('SELECT TOP (1000) [O&M_Code],[Webhook] FROM [dbo].[Station_Limits]', connStr)
df_webhook= pd.DataFrame(SQL_Query, columns=['O&M_Code','Webhook'])

connStr.close()
dflifetime=dflifetime.loc[(dflifetime['Alarm_Status']==1),:]
temp_lifetime=(dflifetime.to_dict(orient='records'))

stations={}
for i in temp_lifetime:
    tz=pytz.timezone(timezones[i['Station_Name'].strip()[0:2]])
    stations['['+i['Station_Name'].strip()+'L]']=[i['Station_No_Meters']]
    temp=[]
    for index,j in enumerate(i['Station_Columns'].strip().split(',')):
        if(index>1 and index<(2+i['Station_No_Meters'])): #Skip date and include meters only
            temp2=(j.replace('"','').split('.')[:-1])
            temp.append(' '.join(temp2))
    stations['['+i['Station_Name'].strip()+'L]'].append(temp)
    stations['['+i['Station_Name'].strip()+'L]'].append([0]*(i['Station_No_Meters']))
    stations['['+i['Station_Name'].strip()+'L]'].append([datetime.datetime.now(tz)]*(i['Station_No_Meters']))


def sendmail(station,type,data,rec):
    SERVER = "smtp.office365.com"
    FROM = 'operations@cleantechsolar.com'
    recipients = rec# must be a list
    cleantech_rec=[]
    for i in recipients:
        if(i.split('@')[1]=='cleantechsolar.com' or i.split('@')[1]=='comin.com.kh'):
            cleantech_rec.append(i)
    TO=", ".join(cleantech_rec)
    SUBJECT = station+' Network Issue/Power Trip Alarm'
    text2='Last Timestamp Read: '+str(data[0]) +'\n\n Last Power Reading: '+str(data[2])+'\n\n Meter Name: '+str(data[1])
    TEXT = text2
    # Prepare actual message
    message = "From:"+FROM+"\nTo:"+TO+"\nSubject:"+ SUBJECT +"\n\n"+TEXT
    # Send the mail
    import smtplib
    server = smtplib.SMTP(SERVER)
    server.starttls()
    server.login('shravan.karthik@cleantechsolar.com', 'CTS&*(789')
    server.sendmail(FROM, recipients, message)
    server.quit()
    try:
        teams_alert(station,data)
    except:
        print('Teams Alert Failed')
    account = "ACcac80891fc3a1edb14f942975a0a8939"
    token = "d39acf2278b2194b59aa15aa9e3a2072"
    client = Client(account, token)
    if(station[1:3]=='KH'):
        smsrec=['+85569313557',"+6591523695",'+85569236207','+85569875847']
        for t in smsrec:
            message = client.messages.create(to="+"+str(int(t)), from_="+12402930809",body=SUBJECT +"\n\n"+TEXT)
    elif(station[1:3]=='TH'):
        smsrec=["+6591523695",'+6585181527','+66870447993','+66928953265',"+66879552308"]
        for t in smsrec:
            message = client.messages.create(to="+"+str(int(t)), from_="+12402930809",body=SUBJECT +"\n\n"+TEXT)


            

def teams_alert(station,data):
    try:
        webhook=df_webhook.loc[df_webhook['O&M_Code'].str.strip()==station[1:-2],'Webhook'].values[0].strip()
        myTeamsMessage = pymsteams.connectorcard(webhook)
        myTeamsMessage.title(station[1:-2]+" Network Issue/Power Trip Alarm")
        myTeamsMessage.text("<pre>Last Timestamp Read: "+str(data[0])+"<br>Last Power Reading: "+str(round(data[2],1))+"<br>Meter Name: "+str(data[1])+"</pre>")
        myTeamsMessage.send()
        time.sleep(5)
    except:
        pass

def rectolist(station,path):
    df=pd.read_csv(path)
    df2=df[['Recipients',station]]
    a=df2.loc[df2[station] == 1]['Recipients'].tolist()
    return a

starttime=time.time()
while(1):
    for j,i in enumerate(stations):
        tz=pytz.timezone(timezones[i[1:3]])
        date_now=datetime.datetime.now(tz).strftime('%Y-%m-%d')
        if(datetime.datetime.now(tz).hour>8 and datetime.datetime.now(tz).hour<18):
            print(i,datetime.datetime.now(tz).strftime("%Y-%m-%d"))
            temp=stations[i][1]
            for index,k in enumerate(temp):
                if(os.path.exists(path+i+'/'+date_now[0:4]+'/'+date_now[0:7]+'/'+k+'/'+i+'-'+k[0:3]+k[4]+'-'+date_now[0:10]+'.txt')):
                    df=pd.read_csv(path+i+'/'+date_now[0:4]+'/'+date_now[0:7]+'/'+k+'/'+i+'-'+k[0:3]+k[4]+'-'+date_now[0:10]+'.txt',sep='\t')
                    #Power Alarm
                    df_power=df[['W_avg']]
                    pow1=df_power.tail(6).sum()
                    last_pow=df_power.tail(1).fillna(0).values[0][0]
                    date=df.tail(1)['ts'].values
                    time_check=datetime.datetime.strptime(date[0],"%Y-%m-%d %H:%M:%S")
                    if(((pow1[0]<1) and stations[i][2][index]==0) and time_check.hour>7):
                        recipients=rectolist(i[1:-1]+'_Mail',mailpath)
                        print('Sending')
                        sendmail(i,'pow',[date[0],k,last_pow],recipients)
                        stations[i][3][index]=datetime.datetime.now(tz)
                        stations[i][2][index]=1
                    elif(((pow1[0]<1) and stations[i][2][index]==1) and time_check.hour>7):
                        diff=datetime.datetime.now(tz)-stations[i][3][index]
                        if((diff.seconds)//60>120):
                            print('Second time')
                            stations[i][3][index]=datetime.datetime.now(tz)
                            recipients=rectolist(i[1:-1]+'_Mail',mailpath)
                            sendmail(i,'pow',[date[0],k,last_pow],recipients)
                    else:
                        stations[i][2][index]==0
    time.sleep(900.0 - ((time.time() - starttime) % 900.0))